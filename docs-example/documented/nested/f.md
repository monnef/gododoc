# GodoDoc Example Project


## documented/nested/f.gd<span id="documented/nested/f"></span>

### func <span id="f--documented/nested/f"></span>`f`()
The `f` function.  

> [DText [DTextPlain "The ",DTextCode "f",DTextPlain " function."]]
<br>

---

Generated by [GodoDoc](https://gitlab.com/monnef/gododoc) at  3. 12. 2021  9:55.