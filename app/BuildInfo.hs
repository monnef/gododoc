module BuildInfo where

import Data.Text (Text)
import Data.Version (showVersion)
import Paths_gododoc
import Utils

versionText :: Text
versionText = showVersion version & toS
