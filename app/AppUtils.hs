{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module AppUtils where

import Data
import Data.Bifunctor (bimap)
import Data.Either (isRight)
import Data.List (intercalate, partition)
import qualified Parser.Data as PD
import Text.InterpolatedString.Perl6 (qq)
import Utils

printParseStats :: [Either Text ParseResult] -> IO ()
printParseStats parsed = do
  let (succP, failP) = partition isRight parsed & bimap length length
  let totalP = length parsed
  let perc = fromIntegral succP / (totalP & fromIntegral) * 100.0
  let sm =
        if  | perc < 10 -> "😢"
            | perc < 30 -> "😕"
            | perc < 50 -> "😐"
            | perc < 75 -> "😐"
            | perc < 99 -> "😉"
            | perc < 100 -> "😁"
            | otherwise -> "😈💯"
  let percF = bool perc 100 (isNaN perc) & formatFloatN 2 & (<> "%")
  tPutStrLn $ sm <> [qq| Successfully parsed: $percF of files ($succP / $failP / $totalP).|]
