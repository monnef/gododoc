{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}

module Main where

import AppUtils
import BuildInfo
import CmdArgsParser
import Configuration
import Control.Lens
import Control.Monad (forM, forM_, void, when)
import Control.Monad.Extra (whenM)
import Crawler
import Data
import Data.Aeson
import Data.Aeson.Encode.Pretty (encodePretty)
import qualified Data.ByteString.Lazy.Char8 as CL
import Data.Default (def)
import Data.Either
  ( fromLeft,
    fromRight,
    isLeft,
    isRight,
    rights,
  )
import Data.Either.Extra (fromEither, fromLeft')
import Data.List (intercalate, partition)
import Data.Maybe (fromMaybe)
import Data.Version (Version, showVersion)
import Generators.Markdown (genMarkdown)
import Parser (parseGDS)
import qualified Parser.Data as PD
import System.Directory.Extra (doesFileExist)
import System.Exit
  ( ExitCode (..),
    exitSuccess,
    exitWith,
  )
import System.FilePath (makeRelative)
import Text.InterpolatedString.Perl6 (qq)
import Text.Pretty.Simple (pPrint)
import Text.Printf (printf)
import Utils

main :: IO ()
main = do
  cmdArgs <- parseCmdArgs
  let ifVerbose = when (cmdArgs ^. verbose)
  when (cmdArgs ^. printVersion) $ do
    tPutStrLn versionText
    exitSuccess
  when (cmdArgs ^. generateConfig) $ do
    let fName = toS defaultConfigFileName
    whenM (doesFileExist fName) $ do
      tErrPutStrLn [qq|File "{fName}" already exists.|]
      exitWith $ ExitFailure 2
    writeFile fName $ toS defaultConfigContents
    exitSuccess
  let printDebug x = ifVerbose $ tPutStrLn x
  printDebug $ "cmdArgs = " <> encode cmdArgs & toS
  let defaultConfig = def @Config
  printDebug $ "default config = " <> encode defaultConfig & toS
  let configFileName = cmdArgs ^. configFilePath & fromMaybe defaultConfigFileName
  loadedConfig <- readConfig configFileName
  printDebug $ "loaded config (" <> configFileName <> ") = " <> ((loadedConfig <&> encode) & tshow)
  configFileExists <- doesFileExist $ toS configFileName
  when (isLeft loadedConfig && configFileExists) $ do
    tErrPutStrLn [qq|Failed to load existing configuration file: {fromLeft "?" loadedConfig}|]
    exitWith $ ExitFailure 3
  let baseConfig = fromRight defaultConfig loadedConfig
  printDebug $ "base config = " <> encode baseConfig & toS
  let config = augmentConfigWithCmdArgs baseConfig cmdArgs
  printDebug $ "config = " <> encode config & toS
  crawlRes <- crawl $ CrawlArgs {_config = config}
  printDebug $ "crawlRes = " <> encodePretty crawlRes & toS
  parsed <-
    forM (crawlRes ^. sourceFiles) $ \filePath -> do
      ifVerbose $ do
        tPutStr $ "Parsing " <> toS filePath <> " ... "
        flushStds
      srcData <- readFile filePath
      let nodes = parseGDS (toS srcData) filePath
      case nodes of
        Left err -> do
          ifVerbose $ do
            tPutStrLn "FAIL"
            flushStds
          tErrPutStrLn err
        Right _ -> ifVerbose $ tPutStrLn "ok"
      let fileName = makeRelative (config ^. inputDir & toS) filePath & toS
      return $ nodes <&> \xs -> ParseResult {_filePath = filePath, _nodes = xs, _fileName = fileName}
  -- pPrint parsed
  printParseStats parsed
  let goodParses = rights parsed
  when (any isLeft parsed) $ do
    tErrPutStrLn "Some files failed to parse, skipping markdown generation."
    exitWith $ ExitFailure 1
  tPutStr "Generating markdown ... "
  flushStds
  genMarkdown GenContext {_config = config, _cmdArgs = cmdArgs} goodParses
  tPutStrLn "ok"
