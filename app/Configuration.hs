{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Configuration where

import Control.Lens
import Data
import Data.Default (def)
import Data.Either.Extra (mapLeft)
import Data.Maybe (fromMaybe)
import qualified Data.Yaml as Y
import Utils

defaultConfigFileName :: Text
defaultConfigFileName = "gododoc.yaml"

defaultConfigContents :: Text
defaultConfigContents = Y.encode (def @Config) & toS

augmentConfigWithCmdArgs :: Config -> CmdArgs -> Config
augmentConfigWithCmdArgs cfg args =
  Config
    { _inputDir = fromMaybe (cfg ^. inputDir) (args ^. inputDir),
      _outputDir = fromMaybe (cfg ^. outputDir) (args ^. outputDir),
      _projectName = fromMaybe (cfg ^. projectName) (args ^. projectName),
      _generateToc = bool (cfg ^. generateToc) False (args ^. dontGenerateToc),
      _multipleFiles = bool (cfg ^. multipleFiles) False (args ^. multipleFiles),
      _omitMdSuffixInLinks = cfg ^. omitMdSuffixInLinks
    }

readConfig :: Text -> IO (Either Text Config)
readConfig fName = do
  c <- readFile (toS fName)
  return $ Y.decodeEither' (toS c) & mapLeft (Y.prettyPrintParseException >>> toS)
