{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TypeApplications #-}

module CmdArgsParser where

import BuildInfo
import Configuration
import Control.Lens
import Data
import Data.Default (def)
import Data.Text (Text)
import qualified Data.Text as T
import Options.Applicative
import qualified Options.Applicative as OA
import Text.InterpolatedString.Perl6 (qq)
import Text.Read (readMaybe)
import Utils

textReader :: ReadM Text
textReader = maybeReader $ T.pack >>> Just

cmdArgsParserInfo :: ParserInfo CmdArgs
cmdArgsParserInfo =
  OA.info
    (cmdArgsParser <**> helper)
    (fullDesc <> progDesc ("GDScript documentation generator created by monnef.\nVersion: " <> toS versionText))

readInt :: String -> Maybe Int
readInt = readMaybe

readText :: String -> Maybe Text
readText = T.pack >>> Just

intMaybeReader :: ReadM (Maybe Int)
intMaybeReader = maybeReader (readInt >>> pure)

textMaybeReader :: ReadM (Maybe Text)
textMaybeReader = maybeReader (readText >>> pure)

cmdArgsParser :: Parser CmdArgs
cmdArgsParser =
  CmdArgs
    <$> switch (long "generate-config" <> genConfigHelp) --
    <*> option textMaybeReader (long "input-dir" <> short 'i' <> value Nothing <> metavar "PATH")
    <*> option textMaybeReader (long "output-dir" <> short 'o' <> value Nothing <> metavar "PATH")
    <*> option textMaybeReader (long "name" <> short 'n' <> value Nothing <> metavar "PROJECTNAME")
    <*> option textMaybeReader (long "config" <> short 'c' <> value Nothing <> metavar "FILENAME" <> configHelp)
    <*> switch (long "verbose" <> short 'v' <> verboseHelp)
    <*> switch (long "version")
    <*> switch (long "no-toc")
    <*> switch (long "multiple-files")
  where
    genConfigHelp = help [qq|Writes default configuration to $defaultConfigFileName|]
    configHelp = help [qq|Set configuration file name (default: $defaultConfigFileName)|]
    verboseHelp = help [qq|Enable verbose output|]

parseCmdArgs :: IO CmdArgs
parseCmdArgs = execParser cmdArgsParserInfo
