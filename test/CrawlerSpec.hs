{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module CrawlerSpec
  ( htf_thisModulesTests,
  )
where

import Control.Lens
import Crawler
import Data
import Test.Framework
import Utils

test_getSourceFilesPaths = do
  let f = getSourceFilesPaths
  r1 <- f "./data/crawler"
  let exp1 = ["./data/crawler/empty.gd", "./data/crawler/subdir/empty2.gd"]
  assertEqual exp1 r1

test_crawl = do
  let f = crawl
  let c =
        CrawlArgs
          { _config =
              Config
                { _inputDir = "./data/crawler",
                  _outputDir = undefined,
                  _projectName = undefined,
                  _generateToc = undefined,
                  _multipleFiles = undefined,
                  _omitMdSuffixInLinks = undefined
                }
          }
  r1 <- f c
  let exp1 = CrawlResult {_sourceFiles = ["./data/crawler/empty.gd", "./data/crawler/subdir/empty2.gd"]}
  assertEqual exp1 r1
