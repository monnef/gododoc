{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module Parser.ParserSpec
  ( htf_thisModulesTests,
  )
where

import Control.Lens
import Control.Monad (when)
import Data
import Data.Either (isLeft)
import Data.Either.Extra (fromLeft')
import qualified Data.Text as T
import Data.Void (Void)
import Parser
import Parser.Common
import Parser.Data
import Parser.ExpressionParser
import Parser.ParserTestUtils
import Parser.StatementParser
import Test.Framework
import Text.InterpolatedString.Perl6 (qq)
import Text.Megaparsec
import Text.Megaparsec.Char (string)
import Utils

test_pPrimType = do
  let p = pPrimType
  assertEqual (Right $ GPrimType PTBool) $ simpleParse p "bool"
  assertEqual (Right $ GPrimType PTInt) $ simpleParse p "int"
  assertEqual (Right $ GPrimType PTFloat) $ simpleParse p "float"
  assertLeft $ simpleParse p ""
  assertLeft $ simpleParse p "gg"
  assertLeft $ simpleParse p " bool"
  assertLeft $ simpleParse p " bool "
  assertLeft $ simpleParse p "bool "

test_pClassType = do
  let p = pClassType
  assertEqual (Right $ GClassType "Frog") $ simpleParse p "Frog"
  assertLeft $ simpleParse p "frog"
  assertEqual (Right $ GClassType "Leaf_7") $ simpleParse p "Leaf_7"
  assertEqual (Right $ GClassType "GGArray") $ simpleParse p "GGArray"
  assertLeft $ simpleParse p " F"
  assertLeft $ simpleParse p " F "
  assertLeft $ simpleParse p "F "

test_pExtends = do
  let p = pExtends
  assertEqual (Right $ NExtends $ GClassType "P") $ simpleParse p "extends P"
  assertEqual (Right $ NExtends $ GClassType "Cow") $ simpleParse p "extends Cow"
  assertEqual (Right $ NExtends $ GClassType "SadCow") $ simpleParse p "extends SadCow"
  assertEqual (Right $ NExtends $ GClassType "Happy_Clown__Cow") $ simpleParse p "extends   Happy_Clown__Cow"
  assertLeft $ simpleParse p " extends A"
  assertLeft $ simpleParse p "extends A "
  assertLeft $ simpleParse p " extends A "

test_pTool = do
  let p = pTool
  assertEqual (Right NTool) $ simpleParse p "tool"
  assertLeft $ simpleParse p "Tool"
  assertLeft $ simpleParse p " tool"
  assertLeft $ simpleParse p " tool "
  assertLeft $ simpleParse p "tool\n"

test_pPass = do
  let p = pPass
  assertEqual (Right NPass) $ simpleParse p "pass"

test_pComment = do
  let p = pComment
  assertEqual (Right $ NComment "TODO") $ simpleParse p "#TODO"
  assertEqual (Right $ NComment " TODO") $ simpleParse p "# TODO"
  assertEqual (Right $ NComment "") $ simpleParse p "#"
  assertEqual (Right $ NComment "green frog") $ simpleParse p "#green frog"
  assertEqual (Right $ NComment "green frog") $ simpleParse p "        #green frog"
  assertLeft $ simpleParse p ""
  assertLeft $ simpleParse p "TODO"
  assertLeft $ simpleParse p "#TODO\n"
  assertLeft $ simpleParse p "##TODO"

test_pCommentStatement = do
  let p = pCommentStatement
  assertEqual (Right $ NComment "TODO") $ simpleParse p "#TODO"
  assertEqual (Right $ NComment "green frog") $ simpleParse p "        #green frog"
  assertRight $ simpleParse p "#TODO\n"
  assertRight $ simpleParse p "#TODO  \n\n"

test_pClassName = do
  let p = pClassName
  assertEqual (Right $ NClassName $ GClassType "WakingSand") $ simpleParse p "class_name WakingSand"
  assertEqual (Right $ NClassName $ GClassType "GGArray") $ simpleParse p "class_name GGArray"
  assertLeft $ simpleParse p "class_name gg"
  assertLeft $ simpleParse p "class_name gg\n"
  assertLeft $ simpleParse p " class_name gg"
  assertLeft $ simpleParse p "class_name gg "

test_pVarSt = do
  let p = pVarSt
  assertEqual (Right $ NVarDef "a" Nothing Nothing []) $ simpleParse p "var a"
  assertEqual (Right $ NVarDef "X" Nothing Nothing []) $ simpleParse p "var X"
  let r3 = simpleParse p "var b = 1"
  printLeftBundleErr r3
  assertEqual (Right $ NVarDef "b" Nothing (Just (ELit (LInt 1))) []) r3
  assertLeft $ simpleParse p "var X\n"

test_pVarSt_typed = do
  let p = simpleParse pVarSt
  let r1 = p "var a: bool"
  printLeftBundleErr r1
  assertEqual (Right $ NVarDef "a" (Just $ GPrimType PTBool) Nothing []) r1
  assertEqual (Right $ NVarDef "b" (Just $ GPrimType PTFloat) (Just (ELit (LFloat 0.1))) []) $ p "var b: float = 0.1"
  assertEqual (Right $ NVarDef "bb" (Just $ GClassType "Cls") (Just (ELit (LFloat 0.1))) []) $ p "var bb: Cls = 0.1"
  assertEqual (Right $ NVarDef "c" Nothing (Just (ELit (LFloat 0.1))) []) $ p "var c:= 0.1"
  assertEqual (Right $ NVarDef "d" Nothing (Just (ELit (LFloat 0.1))) []) $ p "var d := 0.1"
  assertLeft $ p "var X: = 4.0\n"
  assertEqual
    (Right $ NVarDef "c" (Just $ GClassType "Enemy") (Just $ EFuncCall (EAttrRef (EVarRef "Enemy") "new") []) [])
    $ p "var c: Enemy = Enemy.new()"

test_pVarSt_multilineExpr = do
  let p = simpleParse pVarSt
  let r1 = p "var _save_data = {\n              levels = []\n              }\n"
  printLeftBundleErr r1
  assertEqual
    (Right $ NVarDef "_save_data" Nothing (Just (ELit (LDictionary [(ELit $ LString "levels", ELit (LArray []))]))) [])
    r1

test_pVarSt_export = do
  let p = simpleParse pVarSt
  assertEqual (Right $ NVarDef "simple" Nothing Nothing [ExportVDM ExportTypeSimple]) $ p "export var simple"
  assertEqual (Right $ NVarDef "bool_" Nothing Nothing [ExportVDM $ ExportTypeOther $ GPrimType PTBool]) $
    p "export(bool) var bool_"
  assertEqual (Right $ NVarDef "resource" Nothing Nothing [ExportVDM $ ExportTypeOther $ GClassType "Resource"]) $
    p "export(Resource) var resource"
  assertEqual (Right $ NVarDef "arr" Nothing Nothing [ExportVDM $ ExportTypeArray $ GPrimType PTInt]) $
    p "export(Array, int) var arr"
  assertEqual (Right $ NVarDef "arr" Nothing Nothing [ExportVDM $ ExportTypeArray $ GClassType "Resource"]) $
    p "export(Array, Resource) var arr"
  assertEqual (Right $ NVarDef "str_enum_empty" Nothing Nothing [ExportVDM $ ExportTypeOther $ GClassType "String"]) $
    p "export(String) var str_enum_empty"
  assertEqual (Right $ NVarDef "str_enum" Nothing Nothing [ExportVDM $ ExportTypeEnum ["a", "b b"]]) $
    p "export(String, \"a\", \"b b\") var str_enum"

test_pVarSt_onReady = do
  let p = simpleParse pVarSt
  assertEqual (Right $ NVarDef "a" Nothing (Just $ ELit $ LGetNode "A") [OnReadyVDM]) $ p "onready var a = $A"
  assertEqual (Right $ NVarDef "_potion" Nothing (Just $ ELit $ LGetNode "../HPPotion") [OnReadyVDM]) $
    p "onready var _potion = $\"../HPPotion\""

test_pAssign = do
  let p = simpleParse pAssign
  assertEqual (Right $ NAssign (EVarRef "x") "=" $ ELit $ LInt 2) $ p "x=2"
  assertEqual (Right $ NAssign (EVarRef "x") "=" $ ELit $ LInt 2) $ p "x = 2"
  assertEqual (Right $ NAssign (EVarRef "x") "=" $ ELit $ LInt 2) $ p "x = 2 "
  let r4 = p "_Zzz += \"!\""
  printLeftBundleErr r4
  assertRightNoShow r4
  assertEqual (Right $ NAssign (EVarRef "_Zzz") "+=" $ ELit $ LString "!") r4
  assertEqual (Right $ NAssign (EAttrRef (EVarRef "_z") "z") "*=" (ELit (LInt 7))) $ p "_z.z *= 7"

test_pFuncHead = do
  let p = pFuncDefHead
  assertEqual (Right (FuncTypeMethod, "a", [], Nothing)) $ simpleParse p "func a():"
  assertRight $ simpleParse p "func a(): "
  assertLeft $ simpleParse p "func a():\n"
  assertLeft $ simpleParse p "func a(): \n"
  assertEqual (Right (FuncTypeStatic, "a", [], Nothing)) $ simpleParse p "static func a():"

test_pFunc_oneLine = do
  let p = pFunc
  let r1 = simpleParse p "func a(): pass"
  printLeftBundleErr r1
  assertEqual (Right $ NFuncDef FuncTypeMethod "a" [] Nothing [NPass]) r1
  let bArg = FuncDefArg {_name = "b", _typ = Nothing, _defaultValue = Nothing}
  let r2 = simpleParse p "func aa(b): pass"
  printLeftBundleErr r2
  assertEqual (Right $ NFuncDef FuncTypeMethod "aa" [bArg] Nothing [NPass]) r2

test_pFunc_argsTypes = do
  let p = pFunc
  let cArg = FuncDefArg {_name = "c", _typ = Just (GClassType "Enemy"), _defaultValue = Nothing}
  let dArg = FuncDefArg {_name = "d", _typ = Just (GPrimType PTFloat), _defaultValue = Nothing}
  let r1 = simpleParse p "func aaa(d: float, c:Enemy): pass"
  printLeftBundleErr r1
  assertEqual (Right $ NFuncDef FuncTypeMethod "aaa" [dArg, cArg] Nothing [NPass]) r1

test_pFunc_retType = do
  let p = pFunc
  let dArg = FuncDefArg {_name = "d", _typ = Just (GPrimType PTFloat), _defaultValue = Nothing}
  let r1 = simpleParse p "func aaa(d: float) -> String: return str(d)"
  printLeftBundleErr r1
  assertEqual
    ( Right $
        NFuncDef
          FuncTypeMethod
          "aaa"
          [dArg]
          (Just $ GClassType "String")
          [NReturn $ Just (EFuncCall (EVarRef "str") [EVarRef "d"])]
    )
    r1
  let r2 = simpleParse p "func aaa(d: float) -> Frog: return Frog.new(d)"
  printLeftBundleErr r2
  assertEqual
    ( Right $
        NFuncDef
          FuncTypeMethod
          "aaa"
          [dArg]
          (Just $ GClassType "Frog")
          [NReturn (Just $ EFuncCall (EAttrRef (EVarRef "Frog") "new") [EVarRef "d"])]
    )
    r2
  let r3 = simpleParse p "func F() -> void: pass"
  printLeftBundleErr r3
  assertEqual (Right $ NFuncDef FuncTypeMethod "F" [] (Just $ GPrimType PTVoid) [NPass]) r3

test_pSomeTillEol = do
  let p = simpleParse pSomeTillEol
  assertRight $ p "x"
  assertRight $ p "xxx"
  assertLeft $ p ""
  assertLeft $ p "xxx\n"
  assertLeft $ p "\n"

test_pFunc_multiLine_empty = do
  let p = simpleParse pFunc
  let cArg = FuncDefArg {_name = "c", _typ = Just (GClassType "Enemy"), _defaultValue = Nothing}
  let dArg = FuncDefArg {_name = "d", _typ = Just (GPrimType PTFloat), _defaultValue = Nothing}
  let r1 = p "func multiLine(d: float, c:Enemy):\n  pass"
  printLeftBundleErr r1
  assertEqual (Right $ NFuncDef FuncTypeMethod "multiLine" [dArg, cArg] Nothing [NPass]) r1

test_pFunc_multiLine_oneLevel = do
  let p = simpleParse pFunc
  assertRight $ p "func m():\n  if x: pass"
  assertRight $ p "func m():\n  print(\"hello\")\n  pass"
  assertLeft $ p "func m():\npass"

test_pFunc_multiLine_moreLevels = do
  let p = simpleParse pFunc
  assertRight $ p "func m():\n  if x:\n    if y:\n      pass"

test_pFunc_notIndentedComment = do
  let p = simpleParse pFunc
  let r1 = p "func i():\n# awkwardly placed comment\n  pass"
  printLeftBundleErr r1
  assertEqual (Right $ NFuncDef FuncTypeMethod "i" [] Nothing [NPass]) r1

test_pClassDefHead = do
  let p = simpleParse pClassDefHead
  assertEqual (Right "Frog") $ p "class Frog:"

test_pClass = do
  let p = simpleParse pClass
  assertEqual (Right $ NInnerClass "T0" [NConstDef "s" Nothing (Just (ELit (LInt 69)))]) $ p "class T0:\n  const s = 69"

test_pReturn = do
  let p = simpleParse pReturn
  assertEqual (Right $ NReturn $ Just $ ELit $ LInt 1) $ p "return 1"
  assertEqual (Right $ NReturn Nothing) $ p "return"
  assertEqual (Right $ NReturn Nothing) $ p "return "

test_pExpressionAsStatement = do
  let p = simpleParse pExpressionAsStatement
  assertEqual (Right $ NExpr $ EFuncCall (EVarRef "a") []) $ p "a()"
  assertLeft $ p "if()"

test_pIf_onlyIf = do
  let p = simpleParse pIf
  assertEqual (Right $ NIf [(ELit $ LBool True, [NPass])] Nothing) $ p "if true: pass"
  assertEqual
    ( Right $
        NIf [(ELit $ LBool True, [NVarDef "x" Nothing (Just (ELit (LInt 1))) [], NReturn $ Just (EVarRef "x")])] Nothing
    )
    $ p "if true:\n  var x = 1\n  return x"

test_pIf_withElse = do
  let p = simpleParse pIf
  let r1 = p "if true: pass\nelse: return x"
  printLeftBundleErr r1
  assertEqual (Right $ NIf [(ELit $ LBool True, [NPass])] (Just [NReturn $ Just (EVarRef "x")])) r1
  assertEqual (Right $ NIf [(ELit $ LBool True, [NPass])] (Just [NReturn $ Just (EVarRef "x")])) $
    p "if true:\n  pass\nelse:\n  return x"

test_pIf_withElif = do
  let p = simpleParse pIf
  assertEqual
    ( Right $
        NIf
          [(ELit (LBool True), [NPass]), (ELit (LBool False), [NReturn $ Just (EVarRef "e")])]
          (Just [NReturn $ Just (EVarRef "x")])
    )
    $ p "if true: pass\nelif false: return e\nelse: return x"
  let r2 = p "if true:\n  pass\nelif false:\n  print(0)\n  return e\nelif true:\n  print(1)\nelse:\n  return x"
  printLeftBundleErr r2
  assertEqual
    ( Right $
        NIf
          [ (ELit (LBool True), [NPass]),
            (ELit (LBool False), [NExpr (EFuncCall (EVarRef "print") [ELit (LInt 0)]), NReturn $ Just (EVarRef "e")]),
            (ELit (LBool True), [NExpr (EFuncCall (EVarRef "print") [ELit (LInt 1)])])
          ]
          (Just [NReturn $ Just (EVarRef "x")])
    )
    r2

test_pIf_onlyElif = do
  let p = simpleParse pIf
  assertEqual
    ( Right $
        NIf
          [(ELit (LBool True), [NReturn (Just (ELit (LInt 0)))]), (ELit (LBool False), [NReturn (Just (ELit (LInt 1)))])]
          Nothing
    )
    $ p "if true: return 0\nelif false: return 1"
  let r2 = p "if x is float and y is float: return fmod(x, y)\nelif x is int and y is int: return x % y"
  printLeftBundleErr r2
  assertEqual
    ( Right $
        NIf
          [ ( EBAnd (EIs (EVarRef "x") (EGType (GPrimType PTFloat))) (EIs (EVarRef "y") (EGType (GPrimType PTFloat))),
              [NReturn (Just (EFuncCall (EVarRef "fmod") [EVarRef "x", EVarRef "y"]))]
            ),
            ( EBAnd (EIs (EVarRef "x") (EGType (GPrimType PTInt))) (EIs (EVarRef "y") (EGType (GPrimType PTInt))),
              [NReturn (Just (EARem (EVarRef "x") (EVarRef "y")))]
            )
          ]
          Nothing
    )
    r2

test_pIf_commentAfterHead = do
  let p = simpleParse pIf
  assertEqual (Right $ NIf [(EVarRef "_is_dead", [NComment " || _x():", NPass])] Nothing) $
    p "if _is_dead:# || _x():\n  pass"

test_pFor = do
  let p = simpleParse pFor
  assertEqual (Right $ NFor "x" (EVarRef "y") [NPass]) $ p "for x in y: pass"
  assertEqual (Right $ NFor "x" (ELit $ LArray []) [NPass]) $ p "for x in []: pass"
  assertEqual (Right $ NFor "x" (ELit $ LArray []) [NPass]) $ p "for  x  in  [] :   pass  "
  assertEqual (Right $ NFor "x" (ELit $ LArray []) [NPass]) $ p "for x in []:\n  pass"

test_pMatchPattern = do
  let p = simpleParse pMatchPattern
  assertEqual (Right ([MPLit $ LInt 0], [NPass])) $ p "0: pass"
  assertEqual (Right ([MPLit $ LInt 0], [NPass])) $ p "0:\n  pass"
  assertEqual (Right ([MPVar ["X"]], [NPass])) $ p "X: pass"
  assertEqual (Right ([MPVar ["X", "y", "z"]], [NPass])) $ p "X.y.z: pass"
  assertEqual (Right ([MPWildcard], [NPass])) $ p "_: pass"

test_pMatchStatement = do
  let p = simpleParse pMatchStatement
  let r1 = p "match null:\n  true: pass\n  F: return true\n  _:\n    var a\n    pass"
  printLeftBundleErr r1
  assertEqual
    ( Right $
        NMatch
          (ELit LNull)
          [ ([MPLit (LBool True)], [NPass]),
            ([MPVar ["F"]], [NReturn (Just (ELit (LBool True)))]),
            ([MPWildcard], [NVarDef "a" Nothing Nothing [], NPass])
          ]
    )
    r1
  let r2 = p "match null:\n  X.y: pass\n  _: return"
  printLeftBundleErr r2
  assertEqual (Right $ NMatch (ELit LNull) [([MPVar ["X", "y"]], [NPass]), ([MPWildcard], [NReturn Nothing])]) r2

test_pSignal = do
  let p = simpleParse pSignal
  assertEqual (Right $ NSignal "s" []) $ p "signal s"
  assertEqual (Right $ NSignal "ss" []) $ p "signal ss()"
  assertEqual (Right $ NSignal "sss" ["a", "b", "c"]) $ p "signal sss(a, b, c)"

test_pEnum = do
  let p = simpleParse pEnum
  assertEqual (Right $ NEnum Nothing [EnumItem "A" Nothing]) $ p "enum { A }"
  assertEqual (Right $ NEnum (Just "E") [EnumItem "A" Nothing, EnumItem "B" Nothing]) $ p "enum E { A, B }"
  let aMinusOne = EnumItem "A" $ Just $ EANeg $ ELit $ LInt 1
  let bFour = EnumItem "B" $ Just $ ELit $ LInt 4
  assertEqual (Right $ NEnum (Just "E") [aMinusOne, bFour]) $ p "enum E { A = -1, B = 4 }"
  assertEqual (Right $ NEnum (Just "E") [aMinusOne, bFour]) $ p "enum E {\n  A = -1,\n  B = 4\n}"
  assertEqual
    ( Right $
        NEnum
          (Just "E")
          [ EnumItem
              "A"
              ( Just
                  ( EBitOr
                      (EBitShiftLeft (ELit (LInt 1)) (ELit (LInt 2)))
                      (EBitShiftLeft (ELit (LInt 1)) (ELit (LInt 3)))
                  )
              )
          ]
    )
    $ p "enum E { A = 1 << 2 | 1 << 3 }"

test_pWhile = do
  let p = simpleParse pWhile
  assertEqual (Right $ NWhile (ELit $ LBool True) [NPass]) $ p "while true: pass"
  assertEqual (Right $ NWhile (ELit $ LBool True) [NPass]) $ p "while true:\n  pass"
  assertEqual (Right $ NWhile (ELit $ LBool True) [NPass, NPass]) $ p "while true:\n  pass\n  pass"
  assertEqual (Right $ NWhile (ELit $ LBool True) [NPass, NPass]) $ p "while true:\n  pass\n  pass \n"

test_pWhile_breakAndContinue = do
  let p = simpleParse pWhile
  assertEqual (Right $ NWhile (ELit $ LBool True) [NContinue]) $ p "while true: continue"
  assertEqual (Right $ NWhile (ELit $ LBool True) [NBreak]) $ p "while true: break"

test_pDocText = do
  let p = simpleParse pDocText
  assertEqual (Right $ DText [DTextPlain "Jester Croaker"]) $ p "Jester Croaker"
  assertLeft $ p "x\n"
  assertEqual (Right $ DText [DTextPlain "Jester ", DTextRef "Croaker" Nothing]) $ p "Jester [[Croaker]]"
  assertEqual (Right $ DText [DTextPlain "Jester ", DTextRef "Croaker" (Just "croaker")]) $
    p "Jester [[Croaker|croaker]]"
  assertEqual (Right $ DText [DTextPlain "Jester ", DTextRef "Croaker" (Just "croaker"), DTextPlain "."]) $
    p "Jester [[Croaker|croaker]]."
  assertEqual (Right $ DText [DTextCode "Croaker"]) $ p "`Croaker`"
  assertEqual (Right $ DText [DTextPlain "Jester ", DTextCode "Croaker"]) $ p "Jester `Croaker`"

test_pDocParam = do
  let p = simpleParse pDocParam
  assertEqual (Right $ DParam "arr" (DTRef "Array" [DTRef "T" []]) [DTextPlain "array"]) $
    p "@param arr {Array<T>} array"

test_pDocFileDocumentation = do
  let p = simpleParse pDocFileDocumentation
  assertEqual (Right DFileDocumentation) $ p "@fileDocumentation"

test_pDocTypeRaw = do
  let p = simpleParse pDocTypeRaw
  assertEqual (Right $ DTPrim "null") $ p "null"
  assertEqual (Right $ DTPrim "int") $ p "int "
  assertEqual (Right $ DTRef "T" []) $ p "T"
  assertEqual (Right $ DTRef "Array" [DTPrim "int"]) $ p "Array<int>"
  assertEqual (Right $ DTUnion [DTPrim "int", DTPrim "bool"]) $ p "int | bool"
  assertEqual (Right $ DTRef "Array" [DTUnion [DTRef "T" [], DTPrim "null"]]) $ p "Array<T | null>"

test_pDoc = do
  let p = simpleParse pDoc
  assertEqual (Right $ NDoc [DText [DTextPlain "x"]]) $ p "##x"
  assertEqual (Right $ NDoc [DText [DTextPlain "x"]]) $ p "## x"
  assertRight $ p "## x\n"
  assertEqual (Right $ NDoc [DFileDocumentation]) $ p "## @fileDocumentation"

test_pDoc_emptyComment = do
  let p = simpleParse pDoc
  let r = p "## x\n##\n## y"
  printLeftBundleErr r
  assertEqual (Right $ NDoc [DText [DTextPlain "x"], DText [], DText [DTextPlain "y"]]) r

test_optScnSkipComments = do
  let r = parse (optScnSkipComments <* string "## x" <* eof) "" "\n\n## x"
  printLeftBundleErr r
  assertRight r

------------------------------------------------------------------------------------------------------------------------
--
test_pFuncStatements_trailingSemicolon = do
  let p = simpleParse pFuncStatements
  assertEqual (Right [NPass]) $ p "pass"
  assertEqual (Right [NPass]) $ p "pass "
  let r3 = p "pass;"
  printLeftBundleErr r3
  assertRightNoShow r3
  assertEqual (Right [NPass]) r3
  assertEqual (Right [NPass]) $ p "pass; "

test_pTopLevelStatements_trailingSemicolon = do
  let p = simpleParse pTopLevelStatements
  assertEqual (Right [NSignal "a" []]) $ p "signal a()"
  assertEqual (Right [NSignal "a" []]) $ p "signal a() "
  let r3 = p "signal a();"
  printLeftBundleErr r3
  assertRightNoShow r3
  assertEqual (Right [NSignal "a" []]) r3
  let r4 = p "signal a();\n"
  printLeftBundleErr r4
  assertRightNoShow r4
  assertEqual (Right [NSignal "a" []]) r4

test_pTopLevelStatements_doc = do
  let p = simpleParse $ many pTopLevelStatements <&> concat
  assertEqual (Right [NDoc [DText [DTextPlain "x"]]]) $ p "## x"
  assertEqual (Right [NDoc [DText [DTextPlain "x"]]]) $ p "## x\n"
  let r3 = p "extends A\n\n## x"
  printLeftBundleErr r3
  assertEqual (Right [NExtends $ GClassType "A", NDoc [DText [DTextPlain "x"]]]) r3

------------------------------------------------------------------------------------------------------------------------
--
test_parseGDS_emptyWithoutNL = do
  let inp = "extends Node2D"
  assertBool (not $ T.isInfixOf "\n" inp)
  let exp = Right [NExtends $ GClassType "Node2D"]
  let res = parseGDS inp ""
  printLeft res
  assertEqual exp res

test_parseGDS_emptyWithNL = do
  let parseGDSn = "data/undocumented/emptyWithNL.gd"
  inp <- tReadFile parseGDSn
  assertBool (T.isInfixOf "\n" inp)
  let exp = Right [NExtends $ GClassType "Node2D"]
  let res = parseGDS inp (toS parseGDSn)
  printLeft res
  assertEqual exp res

test_parseGDS_emptyWithComment = do
  let parseGDSn = "data/undocumented/emptyWithComment.gd"
  inp <- tReadFile parseGDSn
  let exp = Right [NExtends $ GClassType "Resource", NComment "COMMENT"]
  let res = parseGDS inp (toS parseGDSn)
  printLeft res
  assertEqual exp res

test_parseGDS_emptyWithNlAndComment = do
  let parseGDSn = "data/undocumented/emptyWithNlAndComment.gd"
  inp <- tReadFile parseGDSn
  let exp = Right [NExtends $ GClassType "Resource", NComment "COMMENT"]
  let res = parseGDS inp (toS parseGDSn)
  printLeft res
  assertEqual exp res

test_parseGDS_emptyTool = do
  let parseGDSn = "data/undocumented/emptyTool.gd"
  inp <- tReadFile parseGDSn
  let exp = Right [NTool, NExtends $ GClassType "Resource"]
  let res = parseGDS inp (toS parseGDSn)
  printLeft res
  assertEqual exp res

test_parseGDS_variableSimple = do
  let parseGDSn = "data/undocumented/variablesSimple.gd"
  inp <- tReadFile parseGDSn
  let exp =
        Right
          [ NExtends $ GClassType "Node",
            NVarDef "a" Nothing Nothing [],
            NVarDef "b" Nothing (Just (ELit (LString ""))) [],
            NVarDef "c" Nothing (Just (ELit (LInt 1))) []
          ]
  let res = parseGDS inp (toS parseGDSn)
  printLeft res
  assertEqual exp res

test_parseGDS_variableSimpleTyped = do
  let parseGDSn = "data/undocumented/variablesSimpleTyped.gd"
  inp <- tReadFile parseGDSn
  let exp =
        Right
          [ NExtends (GClassType "Node"),
            NVarDef "a" (Just (GPrimType PTBool)) Nothing [],
            NVarDef "b" (Just (GClassType "String")) (Just (ELit (LString ""))) [],
            NVarDef "c" (Just (GClassType "Enemy")) (Just (EFuncCall (EAttrRef (EVarRef "Enemy") "new") [])) [],
            NVarDef "d" (Just (GPrimType PTFloat)) (Just (ELit (LFloat 6.9))) []
          ]
  let res = parseGDS inp (toS parseGDSn)
  printLeft res
  assertEqual exp res

test_parseGDS_const = do
  let parseGDSn = "data/undocumented/const.gd"
  inp <- tReadFile parseGDSn
  let exp =
        Right
          [ NConstDef "a" Nothing (Just (ELit (LString "A"))),
            NConstDef "PI" Nothing (Just (ELit (LFloat 3.14))),
            NConstDef "_PtF" (Just (GPrimType PTBool)) (Just (ELit (LBool True)))
          ]
  let res = parseGDS inp (toS parseGDSn)
  printLeft res
  assertEqual exp res

test_parseGDS_trivial = do
  let parseGDSn = "data/undocumented/trivial.gd"
  inp <- tReadFile parseGDSn
  let exp = Right [NExtends $ GClassType "Node", NFuncDef FuncTypeMethod "_ready" [] Nothing [NPass]]
  let res = parseGDS inp (toS parseGDSn)
  printLeft res
  assertEqual exp res

test_parseGDS_func = do
  let parseGDSn = "data/undocumented/func.gd"
  inp <- tReadFile parseGDSn
  let exp =
        Right
          [ NExtends $ GClassType "Node",
            NFuncDef
              FuncTypeMethod
              "f"
              [ FuncDefArg {_name = "a", _defaultValue = Just (ELit $ LInt 4), _typ = Nothing},
                FuncDefArg {_name = "b", _defaultValue = Nothing, _typ = Nothing},
                FuncDefArg {_name = "c", _defaultValue = Nothing, _typ = Just (GClassType "X")}
              ]
              (Just $ GClassType "Y")
              [ NIf
                  [ ( ECGTE (EVarRef "a") (ELit (LInt 0)),
                      [ NIf
                          [(ECGT (EVarRef "b") (ELit (LInt 4)), [NReturn $ Just (EVarRef "b")])]
                          (Just [NReturn $ Just (EFuncCall (EAttrRef (EVarRef "c") "y") [EANeg (EVarRef "b")])])
                      ]
                    )
                  ]
                  (Just [NReturn $ Just (EFuncCall (EAttrRef (EVarRef "c") "y") [EVarRef "b"])])
              ],
            NFuncDef FuncTypeMethod "F" [] (Just (GPrimType PTVoid)) [NPass],
            NFuncDef FuncTypeMethod "g" [] Nothing [NComment " awkwardly placed comment", NPass],
            NFuncDef FuncTypeMethod "h" [] Nothing [NPass],
            NFuncDef
              FuncTypeMethod
              "_process"
              [FuncDefArg {_name = "delta", _typ = Just (GPrimType PTFloat), _defaultValue = Nothing}]
              (Just (GPrimType PTVoid))
              [ NWhile
                  ( ECLT
                      (EFuncCall (EAttrRef (EVarRef "OS") "get_ticks_msec") [])
                      (EAAdd (EVarRef "t") (EVarRef "_time_max"))
                  )
                  [NBreak]
              ]
          ]
  let res = parseGDS inp (toS parseGDSn)
  printLeft res
  assertEqual exp res

test_parseGDS_funcDefault = do
  let parseGDSn = "data/undocumented/funcDefault.gd"
  inp <- tReadFile parseGDSn
  let exp =
        Right
          [ NExtends (GClassType "Node"),
            NFuncDef
              FuncTypeMethod
              "_str"
              [FuncDefArg {_name = "x", _typ = Nothing, _defaultValue = Just (ELit (LString "default string"))}]
              (Just (GPrimType PTVoid))
              [NPass],
            NFuncDef
              FuncTypeMethod
              "_str2"
              [FuncDefArg {_name = "x", _typ = Nothing, _defaultValue = Just (ELit (LString ","))}]
              (Just (GPrimType PTVoid))
              [NPass],
            NFuncDef
              FuncTypeMethod
              "_str3"
              [ FuncDefArg {_name = "x", _typ = Nothing, _defaultValue = Just (ELit (LString ","))},
                FuncDefArg {_name = "y", _typ = Nothing, _defaultValue = Just (ELit (LBool False))}
              ]
              (Just (GPrimType PTVoid))
              [NPass],
            NFuncDef
              FuncTypeMethod
              "_str4"
              [ FuncDefArg {_name = "x", _typ = Nothing, _defaultValue = Just (ELit (LString "\""))},
                FuncDefArg {_name = "y", _typ = Nothing, _defaultValue = Nothing}
              ]
              (Just (GPrimType PTVoid))
              [NPass],
            NFuncDef
              FuncTypeMethod
              "_str5"
              [ FuncDefArg {_name = "x", _typ = Nothing, _defaultValue = Just (ELit (LString "\\"))},
                FuncDefArg {_name = "y", _typ = Nothing, _defaultValue = Nothing}
              ]
              (Just (GPrimType PTVoid))
              [NPass],
            NFuncDef
              FuncTypeMethod
              "map"
              [ FuncDefArg {_name = "f", _typ = Nothing, _defaultValue = Nothing},
                FuncDefArg
                  { _name = "ctx",
                    _typ = Nothing,
                    _defaultValue = Just (EAttrRef (EVarRef "GGI") "_EMPTY_CONTEXT")
                  }
              ]
              (Just (GClassType "GGArray"))
              [ NReturn $
                  Just
                    ( EFuncCall
                        (EVarRef "_w")
                        [EFuncCall (EAttrRef (EVarRef "GGI") "map_") [EVarRef "_val", EVarRef "f", EVarRef "ctx"]]
                    )
              ],
            NFuncDef
              FuncTypeMethod
              "format_datetime_"
              [FuncDefArg {_name = "date", _typ = Nothing, _defaultValue = Just (ELit LNull)}]
              (Just (GClassType "String"))
              [ NIf
                  [ ( EBNot (EVarRef "date"),
                      [NAssign (EVarRef "date") "=" (EFuncCall (EAttrRef (EVarRef "OS") "get_datetime") [])]
                    )
                  ]
                  Nothing,
                NReturn $
                  Just
                    ( EARem
                        (ELit (LString "%s-%02d-%02d--%02d-%02d-%02d"))
                        ( ELit
                            ( LArray
                                [ EAttrRef (EVarRef "date") "year",
                                  EAttrRef (EVarRef "date") "month",
                                  EAttrRef (EVarRef "date") "day",
                                  EAttrRef (EVarRef "date") "hour",
                                  EAttrRef (EVarRef "date") "minute",
                                  EAttrRef (EVarRef "date") "second"
                                ]
                            )
                        )
                    )
              ],
            NVarDef
              "format_datetime"
              Nothing
              (Just (EFuncCall (EVarRef "funcref") [EVarRef "self", ELit (LString "format_datetime_")]))
              []
          ]
  let res = parseGDS inp (toS parseGDSn)
  printLeft res
  assertEqual exp res

test_parseGDS_variables = do
  let parseGDSn = "data/undocumented/variablesSetGet.gd"
  inp <- tReadFile parseGDSn
  let exp =
        Right
          [ NExtends (GClassType "Node"),
            NVarDef "hp" Nothing (Just (ELit (LInt 1))) [SetgetVDM (Just "_set_hp", Just "_get_hp")],
            NVarDef "_hp" Nothing (Just (ELit (LInt 1))) [],
            NFuncDef
              FuncTypeMethod
              "_set_hp"
              [FuncDefArg {_name = "x", _typ = Just (GPrimType PTInt), _defaultValue = Nothing}]
              (Just (GPrimType PTVoid))
              [NAssign (EVarRef "_hp") "=" (EVarRef "x")],
            NFuncDef FuncTypeMethod "_get_hp" [] (Just (GPrimType PTInt)) [NReturn $ Just (EVarRef "_hp")],
            NVarDef "val" Nothing Nothing [SetgetVDM (Nothing, Just "_get_val")],
            NFuncDef FuncTypeMethod "_get_val" [] (Just (GClassType "Array")) [NReturn $ Just (ELit (LArray []))],
            NVarDef
              "only_setter"
              (Just (GPrimType PTBool))
              (Just (ELit (LBool True)))
              [SetgetVDM (Just "_set_only_setter", Nothing)],
            NFuncDef FuncTypeMethod "_set_only_setter" [] (Just (GPrimType PTVoid)) [NPass],
            NVarDef
              "only_setter_without_comma"
              (Just (GPrimType PTBool))
              (Just (ELit (LBool True)))
              [SetgetVDM (Just "_set_only_setter", Nothing)]
          ]
  let res = parseGDS inp (toS parseGDSn)
  printLeft res
  assertEqual exp res

test_parseGDS_semicolonStatements = do
  let parseGDSn = "data/undocumented/semicolonStatements.gd"
  inp <- tReadFile parseGDSn
  let exp =
        Right
          [ NExtends (GClassType "A"),
            NClassName (GClassType "B"),
            NFuncDef
              FuncTypeMethod
              "f"
              []
              Nothing
              [NExpr (EFuncCall (EVarRef "print") [ELit (LString "f")]), NReturn (Just (ELit LNull))],
            NVarDef "z" Nothing (Just (ELit (LInt 4))) [],
            NVarDef "zz" Nothing (Just (ELit (LInt 44))) [],
            NFuncDef
              FuncTypeMethod
              "g"
              []
              Nothing
              [ NVarDef "a" Nothing Nothing [],
                NVarDef "b" Nothing Nothing [],
                NVarDef "c" Nothing Nothing [],
                NExpr (EFuncCall (EVarRef "print") [EVarRef "a"]),
                NReturn (Just (EVarRef "c"))
              ],
            NInnerClass "C" [NVarDef "c" Nothing Nothing [], NVarDef "cc" Nothing (Just (EANeg (ELit (LInt 7)))) []]
          ]
  let res = parseGDS inp (toS parseGDSn)
  printLeft res
  assertEqual exp res

test_parseGDS_enum = do
  let parseGDSn = "data/undocumented/enum.gd"
  inp <- tReadFile parseGDSn
  assertBool (T.isInfixOf "\n" inp)
  let exp =
        Right
          [ NExtends (GClassType "Node"),
            NEnum Nothing [EnumItem "A" Nothing],
            NEnum Nothing [EnumItem "A" Nothing, EnumItem "B" Nothing],
            NEnum Nothing [EnumItem "A" (Just $ EANeg $ ELit $ LInt 1), EnumItem "B" (Just $ ELit $ LInt 4)],
            NEnum Nothing [EnumItem "A" (Just $ EANeg $ ELit $ LInt 1), EnumItem "B" (Just $ ELit $ LInt 4)],
            NEnum (Just "E") [EnumItem "A" (Just $ EANeg $ ELit $ LInt 1), EnumItem "B" (Just $ ELit $ LInt 4)],
            NEnum (Just "DamageType") [EnumItem "RANGE" Nothing, EnumItem "MELEE" Nothing],
            NEnum
              (Just "CooldownType")
              [EnumItem "BOMB_TOUCH" Nothing, EnumItem "BOMB_TIMER" Nothing, EnumItem "ATTACK_SMASH" Nothing]
          ]
  let res = parseGDS inp (toS parseGDSn)
  printLeft res
  assertEqual exp res

test_parseGDS_docFunc = do
  let parseGDSn = "data/documented/func.gd"
  inp <- tReadFile parseGDSn
  let exp =
        Right
          [ NExtends (GClassType "Node"),
            NDoc
              [ DText [DTextPlain "Get a random item from an array"],
                DParam "arr" (DTRef "Array" [DTRef "T" []]) [DTextPlain "the input array"],
                DReturn (DTUnion [DTRef "T" [], DTPrim "null"]) [DTextPlain "a random item, or null for an empty array"],
                DTypeParam "T" (DTPrim "any") [DTextPlain "type of items in the array"],
                DExample
                  [ DTextCode "sample_or_null_([1, 2])",
                    DTextPlain " returns ",
                    DTextCode "1",
                    DTextPlain " or ",
                    DTextCode "2",
                    DTextPlain " with equal chance"
                  ],
                DExample [DTextCode "sample_or_null_([])", DTextPlain " returns always ", DTextCode "null"]
              ],
            NFuncDef
              FuncTypeMethod
              "sample_or_null_"
              [FuncDefArg {_name = "arr", _typ = Just (GClassType "Array"), _defaultValue = Nothing}]
              Nothing
              [NReturn (Just (EFuncCall (EAttrRef (EVarRef "GGI") "sample_or_null_") [EVarRef "arr"]))],
            NVarDef
              "sample_or_null"
              Nothing
              (Just (EFuncCall (EVarRef "funcref") [EVarRef "self", ELit (LString "sample_or_null_")]))
              [],
            NDoc
              [ DText
                  [ DTextPlain
                      "Calls first function with given input then sequentially takes a result from a previous function and passes it to a next one."
                  ],
                DText
                  [ DTextPlain "Supports lambdas (string, e.g. ",
                    DTextCode "\"x => x + 1\"",
                    DTextPlain
                      ") and partial application of 2 argument functions (e.g. ",
                    DTextCode "[GG.take, 2]",
                    DTextPlain ")"
                  ],
                DText [DTextPlain "Options dictionary fields:"],
                DText
                  [ DTextPlain "* print - if ",
                    DTextCode "true",
                    DTextPlain " then input, middle values and result is printed"
                  ],
                DExample
                  [ DTextCode "pipe_(0, [inc_, inc_])",
                    DTextPlain " returns ",
                    DTextCode "2",
                    DTextPlain ", it is equivalent to ",
                    DTextCode "inc_(inc_(0))",
                    DTextPlain "."
                  ],
                DExample
                  [ DTextCode "pipe_([1, 2], [[GG.take, 1], \"xs => xs[0] * 10\"])",
                    DTextPlain " returns ",
                    DTextCode "10",
                    DTextPlain ", it is equivalent to ",
                    DTextCode "take_([1, 2], 1)[0] * 10",
                    DTextPlain "."
                  ]
              ],
            NFuncDef
              FuncTypeMethod
              "pipe_"
              [ FuncDefArg
                  { _name = "input",
                    _typ = Nothing,
                    _defaultValue = Nothing
                  },
                FuncDefArg
                  { _name = "functions",
                    _typ = Just (GClassType "Array"),
                    _defaultValue = Nothing
                  },
                FuncDefArg
                  { _name = "options",
                    _typ = Nothing,
                    _defaultValue = Just (ELit LNull)
                  }
              ]
              Nothing
              [ NReturn
                  ( Just
                      ( EFuncCall
                          (EAttrRef (EVarRef "GGI") "pipe_")
                          [EVarRef "input", EVarRef "functions", EVarRef "options"]
                      )
                  )
              ],
            NVarDef
              "pipe"
              Nothing
              ( Just
                  ( EFuncCall
                      (EVarRef "funcref")
                      [EVarRef "self", ELit (LString "pipe_")]
                  )
              )
              []
          ]
  let res = parseGDS inp (toS parseGDSn)
  printLeft res
  assertEqual exp res

test_parseGDS_docAfterExprStatInFunc = do
  let parseGDSn = "data/documented/docAfterExprStatInFunc.gd"
  inp <- tReadFile parseGDSn
  let exp =
        Right
          [ NFuncDef FuncTypeMethod "f" [] Nothing [NExpr (EFuncCall (EAttrRef (EVarRef "a") "c") [])],
            NDoc [DText [DTextPlain "x"]]
          ]
  let res = parseGDS inp (toS parseGDSn)
  printLeft res
  assertEqual exp res
