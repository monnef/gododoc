module Parser.ParserTestUtils where

import Control.Lens
import Control.Monad (when)
import Data
import Data.Either (isLeft)
import Data.Either.Extra (fromLeft')
import qualified Data.Text as T
import Data.Void (Void)
import Parser
import Parser.Common
import Parser.Data
import Parser.ExpressionParser
import Parser.StatementParser
import Test.Framework
import Text.InterpolatedString.Perl6 (qq)
import Text.Megaparsec
import Utils

simpleParse :: Parser a -> Text -> Either (ParseErrorBundle Text Void) a
simpleParse p t = parse (p <* eof) "" t

printLeft :: Either Text a -> IO ()
printLeft (Left e) = tPutStrLn e
printLeft _ = skip

printLeftBundleErr (Left x) = tPutStrLn $ x & errorBundlePretty & toS
printLeftBundleErr _ = skip
