{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module Parser.ExpressionParserSpec
  ( htf_thisModulesTests,
  )
where

import Control.Lens
import Control.Monad (when)
import Data
import Data.Either (isLeft)
import Data.Either.Extra (fromLeft')
import qualified Data.Text as T
import Data.Void (Void)
import Parser
import Parser.Common
import Parser.Data
import Parser.ExpressionParser
import Parser.ParserTestUtils
import Parser.StatementParser
import Test.Framework
import Text.InterpolatedString.Perl6 (qq)
import Text.Megaparsec
import Utils

test_pLitString = do
  let p = simpleParse pLitString
  assertEqual (Right $ LString [qq||]) $ p [qq|""|]
  assertEqual (Right $ LString [qq|^_^|]) $ p [qq|"^_^"|]
  assertEqual (Right $ LString [qq|Hello clown world!|]) $ p [qq|"Hello clown world!"|]
  assertEqual (Right $ LString ", \"") $ p "\", \\\"\""
  let r5 = p "\"\\\\s\""
  printLeftBundleErr r5
  assertEqual (Right $ LString "\\s") r5
  assertEqual (Right $ LString "\n") $ p "\"\\n\""

test_pLitStringMultiline = do
  let p = simpleParse pLitStringMultiline
  assertEqual (Right $ LString "") $ p [qq|""""""|]
  assertEqual (Right $ LString "x") $ p [qq|"""x"""|]
  assertEqual (Right $ LString "\nxxx\n  ") $ p "\"\"\"\nxxx\n  \"\"\""

test_pLitInt = do
  let p = simpleParse pLitInt
  assertEqual (Right $ LInt 0) $ p "0"
  assertEqual (Right $ LInt 0) $ p "0  "
  assertEqual (Right $ LInt $ -1) $ p "-1"
  assertEqual (Right $ LInt 1) $ p "+1"
  assertEqual (Right $ LInt $ -69) $ p "-69"

test_pLitFloat = do
  let p = simpleParse pLitFloat
  assertEqual (Right $ LFloat 0) $ p "0.0"
  assertEqual (Right $ LFloat 0.1) $ p "0.1"
  assertEqual (Right $ LFloat $ -0.1) $ p "-0.1"
  assertEqual (Right $ LFloat 3.14) $ p "3.14"
  assertEqual (Right $ LFloat 3.14) $ p "+3.14"
  assertEqual (Right $ LFloat $ -3.14) $ p "-3.14"
  assertEqual (Right $ LFloat $ 0.2) $ p ".2"

test_pLitArray = do
  let p = simpleParse pLitArray
  assertEqual (Right $ LArray []) $ p "[]"
  assertEqual (Right $ LArray []) $ p "[] "
  assertLeft $ p " []"
  assertLeft $ p " [] "
  assertEqual (Right $ LArray [ELit $ LInt 0]) $ p "[0]"
  assertEqual (Right $ LArray [ELit $ LInt 0, ELit $ LInt 1, ELit $ LInt 2]) $ p "[0, 1, 2]"
  assertEqual (Right $ LArray [ELit $ LString ""]) $ p "[\"\"]"
  assertEqual (Right $ LArray [ELit $ LBool True]) $ p "[ true ]"
  assertEqual (Right $ LArray [ELit $ LArray [ELit $ LBool True]]) $ p "[[true]]"

test_pLitArray_multiline = do
  let p = simpleParse pLitArray
  let r1 = p "[\n  0,\n  1,\n  2\n]"
  printLeftBundleErr r1
  assertRightNoShow r1
  assertEqual (Right $ LArray [ELit (LInt 0), ELit (LInt 1), ELit (LInt 2)]) r1
  let r2 = p "[ 0\n, 1\n, 2\n]"
  printLeftBundleErr r2
  assertRightNoShow r2
  assertEqual (Right $ LArray [ELit (LInt 0), ELit (LInt 1), ELit (LInt 2)]) r2

test_pLitArray_multiline_trailingComma = do
  let p = simpleParse pLitArray
  let r1 = p "[ 0,\n  1,\n]"
  printLeftBundleErr r1
  assertRightNoShow r1
  assertEqual (Right $ LArray [ELit (LInt 0), ELit (LInt 1)]) r1

test_pLitDictionary = do
  let p = simpleParse pLitDictionary
  assertEqual (Right $ LDictionary []) $ p "{}"
  assertEqual (Right $ LDictionary [(ELit $ LString "a", ELit (LInt 0))]) $ p "{\"a\": 0}"
  assertEqual (Right $ LDictionary [(ELit $ LString "a", ELit (LInt 0)), (ELit $ LInt 2, ELit (LBool True))]) $
    p "{\n  \"a\": 0,\n  2: true}"
  assertEqual (Right $ LDictionary [(ELit $ LString "a", ELit (LInt 0))]) $ p "{a = 0}"
  let r5 = p "{\n  a = []\n}"
  printLeftBundleErr r5
  assertEqual (Right $ LDictionary [(ELit $ LString "a", ELit (LArray []))]) r5
  assertEqual (Right $ LDictionary [(EAttrRef (EVarRef "D") "E", ELit (LInt 0))]) $ p "{D.E: 0}"

test_pLitGetNode = do
  let p = simpleParse pLitGetNode
  assertEqual (Right $ LGetNode "a") $ p "$a"
  assertEqual (Right $ LGetNode "X1_2/y") $ p "$X1_2/y"
  assertEqual (Right $ LGetNode "../HPPotion") $ p "$\"../HPPotion\""

test_pLit = do
  let p = simpleParse pLit
  assertEqual (Right $ LInt 0) $ p "0"
  assertEqual (Right $ LFloat 0.2) $ p "0.2"
  assertEqual (Right $ LString "o/") $ p "\"o/\""
  assertEqual (Right LNull) $ p "null"
  assertEqual (Right $ LBool True) $ p "true"
  assertEqual (Right $ LBool False) $ p "false "
  assertEqual (Right $ LArray []) $ p "[]"
  assertEqual (Right $ LArray [ELit $ LInt 0]) $ p "[0]"
  assertEqual (Right $ LArray [ELit $ LFloat 3.14, ELit $ LFloat 2.2]) $ p "[3.14, 2.2 ]"
  assertEqual (Right $ LDictionary [(ELit $ LString "a", ELit (LInt 0))]) $ p "{\"a\": 0}"
  assertEqual (Right $ LDictionary [(ELit $ LString "a", ELit (LInt 0)), (ELit $ LString "x_x", ELit (LString ""))]) $
    p "{a = 0,\n\n\nx_x = \"\"}"
  assertEqual (Right $ LString "x") $ p [qq|"""x"""|]
  assertEqual (Right $ LString "\nx\"x\n") $ p "\"\"\"\nx\"x\n\"\"\""
  assertEqual (Right $ LGetNode "X1_2/y") $ p "$X1_2/y"

test_pVarRef = do
  let p = simpleParse pVarRef
  assertEqual (Right $ EVarRef "x") $ p "x"
  assertEqual (Right $ EVarRef "x") $ p "x "
  assertEqual (Right $ EVarRef "_X") $ p "_X"
  assertEqual (Right $ EVarRef "Enemy") $ p "Enemy"
  assertLeft $ p " x"
  assertLeft $ p "0"
  assertLeft $ p ".x"

test_pExpr_array = do
  let p = simpleParse pExpr
  assertEqual (Right $ ELit $ LArray []) $ p "[]"

test_pExpr_attrRef = do
  let p = simpleParse pExpr
  assertEqual (Right $ EAttrRef (EVarRef "Enemy") "new") $ p "Enemy.new"
  assertEqual (Right $ EAttrRef (EVarRef "Enemy") "new") $ p "Enemy.new  "
  assertEqual (Right $ EAttrRef (EVarRef "_x") "_Y") $ p "_x._Y"
  assertEqual (Right $ EAttrRef (EAttrRef (EAttrRef (EVarRef "a") "b") "c") "d") $ p "a.b.c.d"

test_pExpr_funcCall = do
  let p = simpleParse pExpr
  assertEqual (Right $ EFuncCall (EVarRef "f") []) $ p "f()"
  assertEqual (Right $ EFuncCall (EAttrRef (EVarRef "Enemy") "new") []) $ p "Enemy.new()"
  assertEqual (Right $ EFuncCall (EAttrRef (EVarRef "GG") "take_") [EVarRef "x", ELit (LInt 4)]) $ p "GG.take_(x, 4)"

test_pExpr_funcCall_multiline = do
  let p = simpleParse pExpr
  assertEqual (Right $ EFuncCall (EVarRef "f") [ELit $ LInt 1]) $ p "f(\n  1\n)"
  assertEqual (Right $ EFuncCall (EVarRef "f") [ELit $ LInt 1, EVarRef "a"]) $ p "f(\n  1,\n  a\n)"

test_pExpr_accessFieldOfCallRes = do
  let p = simpleParse pExpr
  let r1 = p "a().x"
  printLeftBundleErr r1
  assertEqual (Right $ EAttrRef (EFuncCall (EVarRef "a") []) "x") $ r1

test_pExpr_accessFieldOfIndexRef = do
  let p = simpleParse pExpr
  let r1 = p "x[y].z"
  printLeftBundleErr r1
  assertEqual (Right $ EAttrRef (EIndexRef (EVarRef "x") (EVarRef "y")) "z") r1

test_pExpr_funcCall_double = do
  let p = simpleParse pExpr
  let r1 = p "preload(\"GGInternal.gd\").new()"
  printLeftBundleErr r1
  assertEqual
    (Right $ EFuncCall (EAttrRef (EFuncCall (EVarRef "preload") [ELit (LString "GGInternal.gd")]) "new") [])
    r1

test_pExpr_indexRef = do
  let p = simpleParse pExpr
  assertEqual (Right $ EIndexRef (EVarRef "x") (EVarRef "y")) $ p "x[y]"
  assertEqual (Right $ EIndexRef (EVarRef "x") (EVarRef "y")) $ p "x[y] "
  assertEqual (Right $ EIndexRef (EVarRef "x") (EVarRef "y")) $ p "x[ y ] "
  assertEqual (Right $ EIndexRef (EVarRef "x") (ELit $ LString "y")) $ p "x[\"y\"]"
  assertEqual (Right $ EIndexRef (EVarRef "x") (ELit $ LArray [])) $ p "x[[]] "
  assertEqual (Right $ EAttrRef (EIndexRef (EVarRef "x") (EVarRef "y")) "z") $ p "x[y].z"
  assertEqual (Right $ EFuncCall (EAttrRef (EIndexRef (EVarRef "x") (EVarRef "y")) "z") []) $ p "x[y].z()"

test_pExpr_basicMath = do
  let p = simpleParse pExpr
  let vrA = EVarRef "a"
  let vrB = EVarRef "b"
  let vrC = EVarRef "c"
  let i4 = ELit (LInt 4)
  assertEqual (Right $ EAAdd vrA vrB) $ p "a + b"
  assertEqual (Right $ EASub vrA vrB) $ p "a - b"
  assertEqual (Right $ EAMul vrA vrB) $ p "a * b"
  assertEqual (Right $ EADiv vrA vrB) $ p "a / b"
  assertEqual (Right $ EARem vrA vrB) $ p "a % b"
  assertEqual (Right $ EASub (EAAdd vrA (EAMul vrB vrC)) i4) $ p "a + b * c - 4"

test_pExpr_comp = do
  let p = simpleParse pExpr
  let vrA = EVarRef "a"
  let vrB = EVarRef "b"
  let vrC = EVarRef "c"
  let i4 = ELit (LInt 4)
  assertEqual (Right $ ECLT vrA vrB) $ p "a < b"
  assertEqual (Right $ ECGT vrA vrB) $ p "a>b"
  assertEqual (Right $ ECEQ vrA vrB) $ p "a == b"
  assertEqual (Right $ ECNEQ vrA vrB) $ p "a != b"
  assertEqual (Right $ ECLTE vrA vrB) $ p "a <= b"
  assertEqual (Right $ ECGTE vrA vrB) $ p "a >= b"

test_pExpr_bool = do
  let p = simpleParse pExpr
  let vrA = EVarRef "a"
  let vrB = EVarRef "b"
  let vrC = EVarRef "c"
  assertEqual (Right $ EBNot vrA) $ p "!a"
  assertEqual (Right $ EBNot vrA) $ p "not a"
  assertEqual (Right $ EBAnd vrA vrB) $ p "a && b"
  assertEqual (Right $ EBOr vrA (EBAnd vrB vrC)) $ p "a || b && c"
  assertEqual (Right $ EBOr (EBAnd vrC (EBNot vrA)) vrB) $ p "c && !a || b"
  assertEqual (Right $ EBOr (EBAnd vrC (EBNot vrA)) vrB) $ p "c and !a or b"

test_pExpr_bit = do
  let p = simpleParse pExpr
  let vrA = EVarRef "a"
  let vrB = EVarRef "b"
  let vrC = EVarRef "c"
  assertEqual (Right $ EBitAnd (EBitShiftLeft vrA vrB) vrC) $ p "a << b & c"
  assertEqual (Right $ EBitOr (EVarRef "a") (EBitXor (EVarRef "b") (EVarRef "c"))) $ p "a | b ^ c"
  assertEqual (Right $ EBitOr (EBitNot (EVarRef "a")) (EBitNot (EVarRef "b"))) $ p "~a | ~b"

test_pExpr_ifElse = do
  let p = simpleParse pExpr
  let vrA = EVarRef "a"
  let vrB = EVarRef "b"
  let vrC = EVarRef "c"
  assertEqual (Right $ EIf vrA vrB vrC) $ p "a if b else c"
  assertEqual
    (Right $ EAAdd vrA (EIf (EAttrRef (EVarRef "Vector2") "ZERO") vrB vrC))
    $p
    "a + (Vector2.ZERO if b else c)"

test_pExpr_parens_simple = do
  let p = simpleParse pExpr
  let vrA = EVarRef "a"
  let vrB = EVarRef "b"
  let vrC = EVarRef "c"
  assertEqual (Right $ EAMul (EAAdd vrA vrB) vrC) $ p "(a + b) * c"

test_pExpr_parens_aroundChain = do
  let p = simpleParse pExpr
  let r1 = p "(a.x).g(0)"
  printLeftBundleErr r1
  assertRightNoShow r1
  assertEqual (Right $ EFuncCall (EAttrRef (EAttrRef (EVarRef "a") "x") "g") [ELit (LInt 0)]) r1
  let r2 = p "(a.x[0].y()).z()[1]"
  printLeftBundleErr r2
  assertRightNoShow r2
  assertEqual
    ( Right $
        EIndexRef
          ( EFuncCall
              (EAttrRef (EFuncCall (EAttrRef (EIndexRef (EAttrRef (EVarRef "a") "x") (ELit (LInt 0))) "y") []) "z")
              []
          )
          (ELit (LInt 1))
    )
    r2

test_pExpr_parens_aroundChain2Real = do
  let p = simpleParse pExpr
  let r1 = p "(to.global_position - from.global_position).normalized()"
  printLeftBundleErr r1
  assertRightNoShow r1
  assertEqual
    ( Right $
        EFuncCall
          ( EAttrRef
              (EASub (EAttrRef (EVarRef "to") "global_position") (EAttrRef (EVarRef "from") "global_position"))
              "normalized"
          )
          []
    )
    r1

test_pExpr_isAndAs = do
  let p = simpleParse pExpr
  let vrA = EVarRef "a"
  assertEqual (Right $ EIs vrA (EGType $ GClassType "Node")) $ p "a is Node"
  assertEqual (Right $ EAs vrA (EGType $ GPrimType PTInt)) $ p "a as int"
  assertEqual (Right $ EAAdd (ELit (LInt 1)) (EAs vrA (EGType $ GPrimType PTInt))) $ p "1 + a as int"

test_pExpr_isAndAs_withLogicOp = do
  let p = simpleParse pExpr
  let r1 = p "(x is float) and (y is float)"
  printLeftBundleErr r1
  assertEqual
    (Right $ EBAnd (EIs (EVarRef "x") (EGType (GPrimType PTFloat))) (EIs (EVarRef "y") (EGType (GPrimType PTFloat))))
    r1
  let r2 = p "x is float and y is float"
  printLeftBundleErr r2
  assertEqual
    (Right $ EBAnd (EIs (EVarRef "x") (EGType (GPrimType PTFloat))) (EIs (EVarRef "y") (EGType (GPrimType PTFloat))))
    r2
