{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module UtilsSpec
  ( htf_thisModulesTests,
  )
where

import Control.Lens
import Test.Framework
import Utils

test_htf = assertBool True

test_capitalizeWord = do
  let f = capitalizeWord
  assertEqual "Abc" $ f "abc"
  assertEqual "Abc" $ f "Abc"
  assertEqual "" $ f ""
  assertEqual "Kira" $ f "kira"
  assertEqual "SISKO" $ f "sISKO"

test_windowed = do
  let f = windowed
  assertEqual [] $ f 2 ([] :: [Int])
  assertEqual [] $ f 2 [1]
  assertEqual [[1]] $ f 1 [1]
  assertEqual [[1, 2]] $ f 2 [1, 2]
  assertEqual [[1, 2], [2, 3]] $ f 2 [1, 2, 3]
  assertEqual [[1, 2], [2, 3], [3, 4]] $ f 2 [1, 2, 3, 4]

test_wrap = do
  let f = Utils.wrap
  assertEqual "xaaax" $ f ("x" :: Text) "aaa"

test_startsWithLowerCase = do
  let f = startsWithLowerCase
  assertBool $ f "aaa"
  assertBool $ f "aAA"
  assertBool $ not $ f "Aaa"
  assertBool $ not $ f "AAA"

test_stripSuffixIfPresent = do
  let f = stripSuffixIfPresent
  assertEqual "" $ f "xxx" ""
  assertEqual "" $ f "xxx" "xxx"
  assertEqual "a" $ f "xxx" "axxx"
