{-# OPTIONS_GHC -F -pgmF htfpp #-}

import {-@ HTF_TESTS @-} UtilsSpec
import {-@ HTF_TESTS @-} CrawlerSpec
import {-@ HTF_TESTS @-} Parser.ParserSpec
import {-@ HTF_TESTS @-} Parser.ExpressionParserSpec

import Test.Framework

main :: IO ()
main = htfMain htf_importedTests
