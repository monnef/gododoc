extends Node

export var a = 1
export(NodePath) var level_node_path
export (Array, NodePath) var main_colliders_paths
export (float) var percent_filled = 1.0 setget _set_percent_filled, _get_percent_filled
