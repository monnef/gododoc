extends Node

func f(a:= 4, b, c:X) -> Y:
    if a >= 0:
        if b > 4:
            return b
        else:
            return c.y(-b)
    else: return c.y(b)

func F() -> void: pass

func g(): # awkwardly placed comment
    pass

func h():
    pass;

func _process(delta: float) -> void:
	while OS.get_ticks_msec() < t + _time_max:
		break
