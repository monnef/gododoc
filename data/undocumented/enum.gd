extends Node

enum { A }
enum { A, B }
enum { A=-1, B = 4 }
enum {
  A=-1,
  B = 4
}
enum E {
  A=-1,
  B = 4
}

enum DamageType {
	RANGE,
	MELEE
}

enum CooldownType {
	BOMB_TOUCH,
	BOMB_TIMER,
	ATTACK_SMASH,
}
