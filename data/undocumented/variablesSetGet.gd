extends Node

var hp := 1 setget _set_hp, _get_hp
var _hp:= 1

func _set_hp(x: int) -> void: _hp = x

func _get_hp() -> int: return _hp

var val setget , _get_val

func _get_val() -> Array: return []

var only_setter: bool = true setget _set_only_setter,

func _set_only_setter() -> void: pass

var only_setter_without_comma: bool = true setget _set_only_setter
