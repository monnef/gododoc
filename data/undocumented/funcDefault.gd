extends Node



func _str(x = "default string") -> void: pass

func _str2(x = ",") -> void: pass
func _str3(x = ",",y = false) -> void: pass
func _str4(x = "\"",y) -> void: pass
func _str5(x = "\\",y) -> void: pass

func map(f, ctx = GGI._EMPTY_CONTEXT) -> GGArray: return _w(GGI.map_(_val, f, ctx))

func format_datetime_(date = null) -> String:
	if !date: date = OS.get_datetime()
	return "%s-%02d-%02d--%02d-%02d-%02d" % [date.year, date.month, date.day, date.hour, date.minute, date.second]
var format_datetime = funcref(self, "format_datetime_")
