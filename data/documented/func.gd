extends Node

## Get a random item from an array
## @param arr {Array<T>} the input array
## @return {T | null}    a random item, or null for an empty array
## @typeParam T {any}    type of items in the array
## @example `sample_or_null_([1, 2])` returns `1` or `2` with equal chance
## @example `sample_or_null_([])` returns always `null`
func sample_or_null_(arr: Array): return GGI.sample_or_null_(arr)
var sample_or_null = funcref(self, "sample_or_null_")

## Calls first function with given input then sequentially takes a result from a previous function and passes it to a next one.
## Supports lambdas (string, e.g. `"x => x + 1"`) and partial application of 2 argument functions (e.g. `[GG.take, 2]`)
## Options dictionary fields:
## * print - if `true` then input, middle values and result is printed
## @example `pipe_(0, [inc_, inc_])` returns `2`, it is equivalent to `inc_(inc_(0))`.
## @example `pipe_([1, 2], [[GG.take, 1], "xs => xs[0] * 10"])` returns `10`, it is equivalent to `take_([1, 2], 1)[0] * 10`.
func pipe_(input, functions: Array, options = null): return GGI.pipe_(input, functions, options)
var pipe = funcref(self, "pipe_")
