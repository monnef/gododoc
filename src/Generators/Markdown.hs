{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}

module Generators.Markdown where

import Control.Lens
import Control.Monad (when)
import Data
import Data.List (find, intercalate, intersperse)
import Data.Maybe
  ( catMaybes,
    fromMaybe,
    isJust,
    isNothing,
  )
import qualified Data.Text as T
import Data.Time
import Data.Tuple.Extra (uncurry3)
import Generators
import Generators.GDScript
import Parser.Data
import System.Directory.Extra (createDirectoryIfMissing)
import System.FilePath ((-<.>), (</>), takeDirectory)
import Text.InterpolatedString.Perl6 (qq)
import Utils

mdExpr :: Expr -> Text
mdExpr = gdExpr

anchorNameForFile :: Text -> Text
anchorNameForFile fileName = fileName & stripSuffixIfPresent ".gd"

anchorNameForFuncOrVar :: Text -> Text -> Text
anchorNameForFuncOrVar fileName funcName = funcName <> "--" <> anchorNameForFile fileName

mdTextPart :: ParseResult -> DTextPart -> Text
mdTextPart _ (DTextPlain x) = x
mdTextPart _ (DTextCode x) = [qq|`{x}`|]
-- TODO: add check if referenced thing exists
-- TODO: support for GDS docs?
mdTextPart pr (DTextRef r t) = mkHashLink text link
  where
    text = fromMaybe r t
    linkFuncInCurrentFile = anchorNameForFuncOrVar (pr ^. fileName) r
    linkToClass = anchorNameForFile r
    link = bool linkToClass linkFuncInCurrentFile $ startsWithLowerCase r

mdDocItems :: ParseResult -> [DocItem] -> Text
mdDocItems pr xs = xs & concatMap (fDocItem pr) & T.intercalate ""
  where
    fDocItem pr (DText xs) = (xs <&> mdTextPart pr) <> ["  \n"]
    fDocItem _ _ = []

mdTyp :: GType -> Text
mdTyp = gdTyp

mdDTyp :: DType -> Text
mdDTyp = gdDTyp

trailingEnterIfNotNull :: Text -> [Text]
trailingEnterIfNotNull x = [x] <> bool ["\n"] [] (x == "")

mdExamples :: GenContext -> ParseResult -> [DocItem] -> Text
mdExamples ctx pr = genList "Examples" fExample
  where
    fExample (DExample text) = (: []) $ mconcat $ text <&> mdTextPart pr
    fExample _ = []

genLabel :: Text -> Text
genLabel label = "**" <> label <> "**: "

genList :: Text -> (a -> [Text]) -> [a] -> Text
genList label gen xs =
  let convertParams = concatMap gen
      joinParams xs = bool (xs & T.intercalate "\\\n" & ((genLabel label <> "\\\n") <>) & (<> "\n")) "" (null xs)
   in xs & convertParams & joinParams

mdDebug :: GenContext -> [DocItem] -> [Text]
mdDebug ctx xs = bool [] ["> ", tshow xs] $ ctx ^. cmdArgs . verbose

mkAnchorForFuncOrVar :: ParseResult -> Text -> (Text, Text)
mkAnchorForFuncOrVar pr n =
  let aName = anchorNameForFuncOrVar (pr ^. fileName) n
   in (mkAnchor aName, aName)

genMdForNode :: GenContext -> ParseResult -> ([DocItem], Node) -> Maybe (Text, [ToCItem])
genMdForNode ctx pr (xs, NFuncDef t n args ret _) =
  Just (textRes, tocRes)
  where
    textRes =
      mconcat $
        ["### func ", anchor, "`", n, "`(", mdArgs args, ")", mdRet ret, "\n"]
          <> trailingEnterIfNotNull (mdDocItems pr xs)
          <> trailingEnterIfNotNull (mdTypeParams xs)
          <> trailingEnterIfNotNull (mdParams xs)
          <> trailingEnterIfNotNull (mdDRet xs)
          <> trailingEnterIfNotNull (mdExamples ctx pr xs)
          <> mdDebug ctx xs
    tocRes = [ToCItem {_objectName = n, _anchorName = anchorName, _fileName = pr ^. fileName}]
    (anchor, anchorName) = mkAnchorForFuncOrVar pr n
    mdArg FuncDefArg {..} =
      mconcat
        [ "`",
          _name,
          "`",
          _typ <&> mdTyp <&> (\x -> ": `" <> x <> "`") & fromMaybe "",
          _defaultValue <&> mdExpr <&> wrap "`" <&> (" = " <>) & fromMaybe ""
        ]
    mdArgs xs = xs <&> mdArg & T.intercalate ", "
    mdRet Nothing = ""
    mdRet (Just x) = " -> `" <> mdTyp x <> "`"
    mdParams = genList "Parameters" fParam
    fParam (DParam n typ text) = (: []) $ mconcat $ ["`", n, "`: `", typ & mdDTyp, "` "] <> (text <&> mdTextPart pr)
    fParam _ = []
    mdDRet xs = xs & concatMap fDRet & T.concat
    fDRet (DReturn typ text) =
      (: []) $ mconcat $ [genLabel "Returns", "`", mdDTyp typ, "` "] <> (text <&> mdTextPart pr) <> ["\n"]
    fDRet _ = []
    mdTypeParams = genList "Type parameters" fTypeParam
    fTypeParam (DTypeParam name typ text) =
      (: []) $ mconcat $ ["`", name, "`: `", typ & mdDTyp, "` "] <> (text <&> mdTextPart pr)
    fTypeParam _ = []
genMdForNode ctx pr d@(xs, NVarDef n t v mods) = Just (textRes, tocRes)
  where
    tocRes = [ToCItem {_objectName = n, _anchorName = anchorName, _fileName = pr ^. fileName}]
    textRes =
      mconcat $
        ["### ", anchor, "var `", n, "`", mdVarType t, "\n"]
          <> trailingEnterIfNotNull (mdDocItems pr xs)
          <> trailingEnterIfNotNull (mdType xs)
          <> trailingEnterIfNotNull (mdExamples ctx pr xs)
          <> mdDebug ctx xs
    (anchor, anchorName) = mkAnchorForFuncOrVar pr n
    mdVarType Nothing = ""
    mdVarType (Just x) = ": `" <> mdTyp x <> "`"
    mdType xs = xs & concatMap fDType & T.concat
    fDType (DType typ text) =
      (: []) $ mconcat $ [genLabel "Type", "`", mdDTyp typ, "` "] <> (text <&> mdTextPart pr) <> ["\n"]
    fDType _ = []
genMdForNode _ _ _ = Nothing

genMdForFile :: GenContext -> ParseResult -> Maybe (Text, ToCFile, ParseResult)
genMdForFile ctx pr@ParseResult {..} =
  if null functionsTexts
    then Nothing
    else
      let text = caption : fileDoc : (functionsTexts & intersperse "---\n\n") & T.intercalate ""
          toc =
            ToCFile
              { _objectName = classNameOrFileName,
                _anchorName = anchorNameForCurrentFile,
                _fileName = _fileName,
                _items = functionsToCItems & concat
              }
       in Just (text, toc, pr)
  where
    caption = "## " <> classNameOrFileName <> anchor <> "\n\n"
    classNameOrFileName = fromMaybe (toS _fileName) $ find isClassName _nodes <&> \(NClassName (GClassType x)) -> x
    anchorNameForCurrentFile = anchorNameForFile $ toS _fileName
    anchor = mkAnchor anchorNameForCurrentFile
    isClassName (NClassName _) = True
    isClassName _ = False
    (functionsTexts, functionsToCItems) = _nodes & filter (not . containsFileDoc) & processDocumentableNodes <&> genMdForNode ctx pr & catMaybes & unzip
    processDocumentableNodes xs = xs & windowed 2 & filter isDocPair <&> \[NDoc x, y] -> (x, y)
    isDocPair [NDoc _, NFuncDef {}] = True
    isDocPair [NDoc _, NVarDef {}] = True
    isDocPair _ = False
    containsFileDoc (NDoc xs) = DFileDocumentation `elem` xs
    containsFileDoc _ = False
    fileDoc =
      let p (NDoc xs) = mdDocItems pr xs <> "\n---\n\n"
          p _ = ""
       in _nodes & filter containsFileDoc <&> p & T.intercalate ""

mkAnchor :: Text -> Text
mkAnchor n = [qq|<span id="{n}"></span>|]

mkLink :: Text -> Text -> Text
mkLink n l = [qq|[{n}]({l})|]

mkHashLink :: Text -> Text -> Text
mkHashLink n l = mkLink n ("#" <> l)

data SimpleToCMode = StmIndexMulti | StmIndexOne | StmOneMulti

genMdTocForFileSimple :: GenContext -> SimpleToCMode -> ToCFile -> Text
genMdTocForFileSimple ctx m = genMdTocForFileSimpleRaw ctx m >>> (<> "\n")

genMdTocForFileSimpleRaw :: GenContext -> SimpleToCMode -> ToCFile -> Text
genMdTocForFileSimpleRaw ctx mode = processFile (getLinkMaker mode)
  where
    processFile linkMaker ToCFile {..} =
      "* " <> linkMaker _objectName _anchorName <> "\n" <> (_items <&> processItem linkMaker & T.intercalate "\n")
    processItem linkMaker ToCItem {..} = "  * " <> linkMaker _objectName _anchorName
    getLinkMaker StmIndexOne = mkHashLink
    getLinkMaker StmOneMulti = mkHashLink
    getLinkMaker StmIndexMulti = mkIndexMultiLink
    mkIndexMultiLink n l = mkLink n (processLinkForIndexMulti l)
    processLinkForIndexMulti l =
      if T.isInfixOf "--" l
        then T.splitOn "--" l & \[objName, fileName] -> fileName <> fileSuffix <> "#" <> l
        else l <> fileSuffix
    fileSuffix = bool ".md" "" (ctx ^. config . omitMdSuffixInLinks)

genMdToCSimple :: GenContext -> SimpleToCMode -> [ToCFile] -> Text
genMdToCSimple ctx m xs = xs <&> genMdTocForFileSimpleRaw ctx m & T.intercalate "\n" & (<> "\n")

genHeader :: GenContext -> Text
genHeader ctx = "# " <> ctx ^. config . projectName <> "\n"

genFooter :: GenContext -> IO Text
genFooter ctx = do
  timeStr <- getCurrentTime <&> formatTime defaultTimeLocale "%e. %-m. %Y %k:%M" <&> toS
  return $ "<br>\n\n---\n\nGenerated by [GodoDoc](https://gitlab.com/monnef/gododoc) at " <> timeStr <> "."

genAndWriteIndexFile :: GenContext -> [ToCFile] -> [Text] -> IO ()
genAndWriteIndexFile ctx tocItems filesResults = do
  let multi = ctx ^. config . multipleFiles
  let outDir = ctx ^. config . outputDir
  createDirectoryIfMissing True (toS outDir)
  let genToc = ctx ^. config . generateToc || multi
  let tocResult = if genToc then genMdToCSimple ctx (bool StmIndexOne StmIndexMulti multi) tocItems else ""
  footer <- genFooter ctx
  let filesPart = bool filesResults [] multi
  let res = genHeader ctx : tocResult : filesPart <> [footer] <&> toS & intercalate "\n"
  let outFileName = outDir <> "/index.md"
  writeFile (toS outFileName) res

genAndWriteOneFile :: GenContext -> Text -> ToCFile -> ParseResult -> IO ()
genAndWriteOneFile ctx fileResult tocItems parseResult = do
  let outDir = ctx ^. config . outputDir
  let outFileName = toS outDir </> (parseResult ^. fileName & toS & (-<.> "md"))
  footer <- genFooter ctx
  let genToc = ctx ^. config . generateToc
  let tocResult = if genToc then genMdTocForFileSimple ctx StmOneMulti tocItems else ""
  let res = [genHeader ctx, tocResult, fileResult, footer] <&> toS & intercalate "\n"
  createDirectoryIfMissing True (takeDirectory outFileName)
  writeFile outFileName res

genMarkdown :: Generator
genMarkdown ctx xs = do
  --
  let perFileResults = xs <&> genMdForFile ctx & catMaybes
  let (filesResults, tocItems, origParseResults) = perFileResults & unzip3
  --
  genAndWriteIndexFile ctx tocItems filesResults
  when (ctx ^. config . multipleFiles) $ zip3 filesResults tocItems origParseResults & mapM_ (uncurry3 $ genAndWriteOneFile ctx)
