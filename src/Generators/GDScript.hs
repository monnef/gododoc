{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Generators.GDScript where

import qualified Data.Text as T
import Parser.Data
import Text.InterpolatedString.Perl6 (qq)
import Utils

-- TODO: rest of expressions?
gdExpr :: Expr -> Text
gdExpr (ELit LNull) = "null"
gdExpr (ELit (LBool x)) = bool "false" "true" x
gdExpr (ELit (LString x)) = tshow x
gdExpr (ELit (LInt x)) = tshow x
gdExpr (ELit (LFloat x)) = tshow x
gdExpr (EAttrRef x y) = gdExpr x <> "." <> y
gdExpr (EVarRef x) = x
gdExpr x = [qq|<<TODO: {tshow x}>>|]

gdTyp :: GType -> Text
gdTyp (GPrimType PTBool) = "bool"
gdTyp (GPrimType PTInt) = "int"
gdTyp (GPrimType PTFloat) = "float"
gdTyp (GPrimType PTVoid) = "void"
gdTyp (GClassType x) = x

gdDTyp :: DType -> Text
gdDTyp (DTPrim x) = x
gdDTyp (DTRef x []) = x
gdDTyp (DTRef x tps) = x <> "<" <> (tps <&> gdDTyp & T.intercalate ", ") <> ">"
gdDTyp (DTUnion ms) = ms <&> gdDTyp & T.intercalate " | "
