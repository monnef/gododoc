{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}

module Utils
  ( tshow,
    tPutStr,
    tPutStrLn,
    tPutStrLnLn,
    tErrPutStrLn,
    tReadFile,
    flushStdOut,
    flushStdErr,
    flushStds,
    (>>>),
    (&),
    (<&>),
    ($>),
    (<$),
    Text,
    bool,
    orCrash,
    convErrToText,
    skip,
    parseDouble,
    liftError,
    capitalizeWord,
    deCapitalizeWord,
    startsWithLowerCase,
    stripSuffixIfPresent,
    toS,
    unzipF,
    unzipMaybeTuple,
    formatFloatN,
    forM,
    forM_,
    void,
    windowed,
    wrap,
  )
where

import Control.Arrow ((>>>))
import qualified Control.Arrow as CA
import Control.Concurrent (threadDelay)
import Control.Exception (SomeException)
import Control.Lens
import Control.Monad (forM, forM_, void)
import Data.Bool (bool)
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Lazy.Char8 as CL
import Data.Char (toLower, toUpper)
import Data.Either.Combinators (mapLeft)
import Data.Function ((&))
import Data.Functor (($>), (<$), (<&>))
import Data.Maybe (fromMaybe)
import Data.String.Conv (toS)
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.Encoding as TLE
import Numeric (showFFloat)
import System.IO (hFlush, hPutStrLn, stderr, stdout)
import Text.Read (readMaybe)

tshow :: Show a => a -> Text
tshow = show >>> T.pack

tPutStr :: Text -> IO ()
tPutStr = T.unpack >>> putStr

tPutStrLn :: Text -> IO ()
tPutStrLn = T.unpack >>> putStrLn

tPutStrLnLn :: Text -> IO ()
tPutStrLnLn x = x <> "\n" & tPutStrLn

tErrPutStrLn :: Text -> IO ()
tErrPutStrLn = toS >>> hPutStrLn stderr

flushStdOut :: IO ()
flushStdOut = hFlush stdout

flushStdErr :: IO ()
flushStdErr = hFlush stderr

flushStds :: IO ()
flushStds = flushStdOut >> flushStdErr

tReadFile :: Text -> IO Text
tReadFile = T.unpack >>> readFile >>> fmap T.pack

orCrash :: Show e => Either e a -> a
orCrash (Right x) = x
orCrash (Left e) = error $ show e

parseDouble :: Text -> Maybe Double
parseDouble = T.unpack >>> readMaybe

convErrToText :: Either SomeException a -> Either Text a
convErrToText = mapLeft tshow

skip :: Monad m => m ()
skip = return ()

liftError :: Either l (Either l r) -> Either l r
liftError (Left x) = Left x
liftError (Right x) = x

deCapitalizeWord :: Text -> Text
deCapitalizeWord "" = ""
deCapitalizeWord x = (x & T.head & toLower & (: []) & T.pack) <> (x & T.tail)

capitalizeWord :: Text -> Text
capitalizeWord "" = ""
capitalizeWord x = (x & T.head & toUpper & (: []) & T.pack) <> (x & T.tail)

startsWithLowerCase :: Text -> Bool
startsWithLowerCase = T.take 1 >>> \x -> T.toLower x == x

stripSuffixIfPresent :: Text -> Text -> Text
stripSuffixIfPresent s x = x & T.stripSuffix s & fromMaybe x

unzipF :: Functor f => f (a, b) -> (f a, f b)
unzipF xs = (fst <$> xs, snd <$> xs)

unzipMaybeTuple :: Maybe (Maybe a, Maybe b) -> (Maybe a, Maybe b)
unzipMaybeTuple Nothing = (Nothing, Nothing)
unzipMaybeTuple (Just (a, b)) = (a, b)

formatFloatN :: Int -> Float -> Text
formatFloatN numOfDecimals floatNum = showFFloat (Just numOfDecimals) floatNum "" & toS

windowed :: Int -> [a] -> [[a]]
windowed _ [] = []
windowed size ls@(x : xs) =
  if length ls >= size
    then take size ls : windowed size xs
    else windowed size xs

wrap :: Semigroup a => a -> a -> a
wrap x y = x <> y <> x
