{-# LANGUAGE OverloadedStrings #-}

module Crawler
  ( crawl,
    getSourceFilesPaths,
  )
where

import Control.Lens
import Control.Monad.Catch (MonadThrow)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Except (throwError)
import Data
import qualified System.FilePath.Find as Find
import Utils

getSourceFilesPaths :: (MonadIO m, MonadThrow m) => FilePath -> m [FilePath]
getSourceFilesPaths path =
  liftIO $ Find.find Find.always (Find.extension Find.==? ".gd") path

crawl :: (MonadIO m, MonadThrow m) => CrawlArgs -> m CrawlResult
crawl args = do
  gdFiles <- getSourceFilesPaths (args ^. (config . inputDir) & toS)
  return $ CrawlResult {_sourceFiles = gdFiles}
