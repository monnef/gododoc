module UtilsTH where

import Control.Lens ((&))
import Control.Lens.Internal.FieldTH (_fieldToDef, makeFieldOptics)
import Control.Lens.TH
import Data.Aeson.TH
  ( constructorTagModifier,
    defaultOptions,
    fieldLabelModifier,
  )
import Data.Char (toLower, toUpper)
import Language.Haskell.TH (DecsQ, Name)

convertFieldName :: String -> String -> String
convertFieldName prefix x = x & drop (length prefix) & deCapitalizeWordStr

deCapitalizeWordStr :: String -> String
deCapitalizeWordStr "" = ""
deCapitalizeWordStr x = (x & head & toLower & (: [])) <> (x & tail)

capitalizeWordStr :: String -> String
capitalizeWordStr "" = ""
capitalizeWordStr x = (x & head & toUpper & (: [])) <> (x & tail)

derJsonOpts = defaultOptions {fieldLabelModifier = drop 1, constructorTagModifier = deCapitalizeWordStr}
