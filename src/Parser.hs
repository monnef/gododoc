{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Parser where

import Data.Either.Extra (mapLeft)
import Parser.Common
import Parser.Data
import Parser.ExpressionParser (pExpr)
import Parser.StatementParser
import Text.Megaparsec
import Utils

pToplevelStatements :: Parser [Node]
pToplevelStatements = do
  optScn
  nodes <- many (pTopLevelStatements & moreWithMaybeCommentStatement) <&> concat
  eof
  return nodes

parseGDS :: Text -> FilePath -> Either Text [Node]
parseGDS input fileName = runParser pToplevelStatements fileName input & mapLeft (errorBundlePretty >>> toS)
