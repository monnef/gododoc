{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}

module Data where

import Control.Lens hiding ((&))
import Control.Monad.IO.Class (MonadIO)
import Data.Aeson.Encoding (text)
import Data.Aeson.Parser (jstring)
import Data.Aeson.TH hiding (Options)
import Data.Aeson.Types
import Data.Default (Default (..))
import Data.Text (Text)
import qualified Data.Text as T
import GHC.Generics (Generic)
import Parser.Data (Node)
import System.FilePath ((</>))
import Utils
import UtilsTH

data Config
  = Config
      { _outputDir :: Text,
        _inputDir :: Text,
        _projectName :: Text,
        _generateToc :: Bool,
        _multipleFiles :: Bool,
        _omitMdSuffixInLinks :: Bool
      }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''Config

deriveJSON derJsonOpts ''Config

instance Default Config where
  def =
    Config
      { _outputDir = "./docs",
        _inputDir = ".",
        _projectName = "My Project",
        _generateToc = True,
        _multipleFiles = True,
        _omitMdSuffixInLinks = False
      }

data CmdArgs
  = CmdArgs
      { _generateConfig :: Bool,
        _inputDir :: Maybe Text,
        _outputDir :: Maybe Text,
        _projectName :: Maybe Text,
        _configFilePath :: Maybe Text,
        _verbose :: Bool,
        _printVersion :: Bool,
        _dontGenerateToc :: Bool,
        _multipleFiles :: Bool
      }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''CmdArgs

deriveJSON derJsonOpts ''CmdArgs

data GenContext
  = GenContext
      { _config :: Config,
        _cmdArgs :: CmdArgs
      }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''GenContext

deriveJSON derJsonOpts ''GenContext

data CrawlArgs
  = CrawlArgs
      { _config :: Config
      }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''CrawlArgs

data CrawlResult
  = CrawlResult
      { _sourceFiles :: [FilePath]
      }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''CrawlResult

deriveJSON derJsonOpts ''CrawlResult

data ParseResult
  = ParseResult
      { _filePath :: FilePath,
        _nodes :: [Node],
        _fileName :: Text
      }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''ParseResult

data ToCItem
  = ToCItem
      { _objectName :: Text,
        _anchorName :: Text,
        _fileName :: Text
      }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''ToCItem

data ToCFile
  = ToCFile
      { _objectName :: Text,
        _anchorName :: Text,
        _fileName :: Text,
        _items :: [ToCItem]
      }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''ToCFile
