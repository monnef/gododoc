module Generators where

import Data
import qualified Parser.Data as PD

type Generator = GenContext -> [ParseResult] -> IO ()
