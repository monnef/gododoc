{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}

module Parser.ExpressionParser
  ( pExpr,
    pVarRef,
    pLit,
    pLitIntRaw,
    pLitInt,
    pLitFloat,
    pLitStringRaw,
    pLitString,
    pLitArray,
    pLitDictionary,
    pLitBool,
    pLitStringMultiline,
    pLitGetNode,
  )
where

import Control.Monad (liftM2)
import Control.Monad.Combinators.Expr
import Data.Maybe (fromMaybe)
import qualified Data.Text as T
import Parser.Common
import Parser.Data
import Text.InterpolatedString.Perl6 (qq)
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L
import Text.Megaparsec.Debug (dbg)
import Utils

pLitIntRaw :: Parser Int
pLitIntRaw = L.signed pNothing (lexeme L.decimal)

pLitInt :: Parser Literal
pLitInt = pLitIntRaw <&> LInt

pLitFloat :: Parser Literal
pLitFloat = L.signed pNothing (lexeme pFloat) >>= (LFloat >>> return)
  where
    pFloat = do
      dec <- optional (some digitChar) <&> fromMaybe "0"
      char '.'
      rest <- some digitChar
      return $ read @Float (dec <> "." <> rest & toS)

pLitStringRaw :: Parser Text
pLitStringRaw = do
  char '"'
  chs <- lexeme $ manyTill L.charLiteral (char '"')
  return $ toS chs

pLitString :: Parser Literal
pLitString = pLitStringRaw <&> LString

pLitStringMultiline :: Parser Literal
pLitStringMultiline = do
  string [qq|"""|]
  chs <- lexeme $ manyTill L.charLiteral (string [qq|"""|]) <&> toS
  return $ LString chs

pLitNull :: Parser Literal
pLitNull = symbol "null" $> LNull

pLitBool :: Parser Literal
pLitBool = do
  vStr <- try (symbol "true") <|> symbol "false"
  let v = bool False True (vStr == "true")
  return $ LBool v

pLitArray :: Parser Literal
pLitArray = do
  symbol "["
  optScnSkipComments
  let sepP = optScn >> symbol "," <* optScnSkipComments
  items <- pExpr `sepEndBy` try sepP
  optScnSkipComments
  symbol "]"
  return $ LArray items

pLitDictionary :: Parser Literal
pLitDictionary = do
  symbol "{" >> optScnSkipComments
  items <- (pItem <* optScnSkipComments) `sepEndBy` (void (char ',') >> optScnSkipComments)
  optScnSkipComments
  symbol "}"
  optScnSkipComments
  return $ LDictionary items
  where
    pItem = try pColonItem <|> pEqualItem
    pColonItem = do
      name <- pExpr
      symbol ":"
      value <- pExpr
      return (name, value)
    pEqualItem = do
      name <- pVarId
      symbol "="
      value <- pExpr
      return (ELit $ LString name, value)

pLitGetNode :: Parser Literal
pLitGetNode = do
  char '$'
  nodePath <- pLitStringRaw <|> pWithoutQuotes
  return $ LGetNode nodePath
  where
    pWithoutQuotes = some (alphaNumChar <|> char '/' <|> char '_' <|> char '-') <&> toS

-- TODO: ? NodePath/StringName lit @"Node/Label"
-- TODO: ? non-10 base numbers 0x, 0b
pLit :: Parser Literal
pLit =
  choice
    [ try pLitNull,
      try pLitBool,
      try pLitFloat,
      try pLitInt,
      try pLitStringMultiline,
      try pLitString,
      try pLitArray,
      try pLitDictionary,
      try pLitGetNode
    ]

pELit :: Parser Expr
pELit = pLit <&> ELit

pVarRef :: Parser Expr
pVarRef = do
  id <- pVarId
  failWhenIdIsDisallowedForEValRef id
  return $ EVarRef id

pExpr :: Parser Expr
pExpr = lexeme pExprViaMakeExprParser & dbg' "pExpr"

pExprViaMakeExprParser = makeExprParser pExprTerm exprTable <?> "expression"

doesntFormAssign :: Parser a -> Parser a
doesntFormAssign p = try (p <* notFollowedBy (char '='))

exprTable =
  [ [Postfix (foldr1 (.) . reverse <$> some (opFuncCall <|> opIndexRef <|> opAttrRef))],
    [Postfix (opBinWithType "is" EIs), Postfix (opBinWithType "as" EAs)],
    [Prefix (EBitNot <$ symbol "~")],
    [Prefix (EANeg <$ symbol "-")],
    [ InfixL (EAMul <$ doesntFormAssign (symbol "*")),
      InfixL (EADiv <$ doesntFormAssign (symbol "/")),
      InfixL (EARem <$ doesntFormAssign (symbol "%"))
    ],
    [InfixL (EAAdd <$ doesntFormAssign (symbol "+"))],
    [InfixL (EASub <$ doesntFormAssign (symbol "-"))],
    [InfixL (EBitShiftLeft <$ symbol "<<"), InfixL (EBitShiftRight <$ symbol ">>")],
    [InfixL (EBitAnd <$ try (symbol "&" >> notFollowedBy (char '&')))],
    [InfixL (EBitXor <$ symbol "^")],
    [InfixL (EBitOr <$ try (symbol "|" >> notFollowedBy (char '|')))],
    [ InfixL (ECLTE <$ symbol "<="),
      InfixL (ECGTE <$ symbol ">="),
      InfixL (ECLT <$ symbol "<"),
      InfixL (ECGT <$ symbol ">"),
      InfixL (ECEQ <$ symbol "=="),
      InfixL (ECNEQ <$ symbol "!=")
    ],
    [InfixL (EIn <$ symbol "in")],
    [Prefix (EBNot <$ (try (symbol "!") <|> symbol "not"))],
    [InfixL (EBAnd <$ (try (symbol "and" & dbg' "and") <|> symbol "&&"))],
    [InfixL (EBOr <$ (try (symbol "or") <|> symbol "||"))],
    [TernR ((EIf <$ symbol "else") <$ symbol "if")]
  ]

pSimpleTerm = try pVarRef <|> pELit

pAttrIndexFuncCallChain =
  liftM2 (foldl (&)) pSimpleTerm (many (try opAttrRef <|> try opIndexRef <|> try opFuncCall))
    & dbg' "pAttrIndexFuncCallChain"

-- non-universal hacky solution for parens
pExprTerm =
  try (parens pAttrIndexFuncCallChain) <|> try (parens pExprViaMakeExprParser) <|> pAttrIndexFuncCallChain <?> "term"

opAttrRef :: Parser (Expr -> Expr)
opAttrRef = do
  symbol "." & dbg' "opAttrRef symbol \".\""
  fieldName <- pVarId & dbg' "opAttrRef pVarId"
  return $ \x -> EAttrRef x fieldName

opFuncCall :: Parser (Expr -> Expr)
opFuncCall = do
  symbol "("
  optScn
  args <- sepBy pExpr (symbol "," <* optScn)
  optScn
  symbol ")" & dbg' "opFuncCall symbol \")\""
  return $ \funcExpr -> EFuncCall funcExpr args

opIndexRef :: Parser (Expr -> Expr)
opIndexRef = do
  symbol "["
  e <- pExpr
  symbol "]" & dbg' "opIndexRef symbol \"]\""
  return $ \obj -> EIndexRef obj e

opBinWithType :: Text -> (Expr -> Expr -> Expr) -> Parser (Expr -> Expr)
opBinWithType sym gen = do
  symbol sym & dbg' ("opBinWithType " <> toS sym)
  e <- pType <&> EGType
  optSc
  return $ \a -> gen a e

dbg' :: Show a => String -> Parser a -> Parser a
dbg' label = bool id (dbg label) enableDebugOutput

enableDebugOutput = False
