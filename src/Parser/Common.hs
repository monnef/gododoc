{-# LANGUAGE OverloadedStrings #-}

module Parser.Common where

import Control.Monad (join, unless, void, when)
import Data.Maybe (fromJust)
import qualified Data.Set as Set
import Parser.Data
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L
import Utils

scn :: Parser ()
scn = L.space space1 empty empty

optScn :: Parser ()
optScn = void $ optional scn

sc :: Parser ()
sc = L.space (void $ some (char ' ' <|> char '\t' <|> (char '\\' <* eol))) empty empty

optSc :: Parser ()
optSc = void $ optional sc

lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

symbol :: Text -> Parser Text
symbol x = lexeme $ string x

parens :: Parser a -> Parser a
parens p = symbol "(" *> try p <* symbol ")"

pClassType :: Parser GType
pClassType = do
  first <- upperChar
  name <- many (alphaNumChar <|> char '_') <&> toS
  return $ GClassType (toS [first] <> name)

pPrimType :: Parser GType
pPrimType = do
  let gdTypeToPT = [("bool", PTBool), ("int", PTInt), ("float", PTFloat), ("void", PTVoid)]
  t <- choice $ gdTypeToPT <&> fst <&> string
  return $ lookup t gdTypeToPT & fromJust & GPrimType

pType :: Parser GType
pType = pClassType <|> pPrimType <?> "type"

pNothing :: Parser ()
pNothing = string "" & void

pVarId :: Parser Text
pVarId = p <?> "identifier"
  where
    p = do
      x <- (letterChar <|> char '_') <&> (: []) <&> toS
      xs <- lexeme (many (alphaNumChar <|> char '_') <&> toS)
      return $ x <> xs

pValidVarId :: Parser Text
pValidVarId = do
  id <- pVarId
  failWhenIdIsDisallowedForEValRef id
  return id

pFuncId :: Parser Text
pFuncId = pVarId

pClassId :: Parser Text
pClassId = pVarId

reservedKeyWords :: [Text]
reservedKeyWords =
  [ "if",
    "elif",
    "else",
    "for",
    "while",
    "match",
    "break",
    "continue",
    "pass",
    "return",
    "class",
    "extends",
    "is",
    "as",
    "self",
    "tool",
    "signal",
    "func",
    "static",
    "const",
    "enum",
    "var",
    "onready",
    "export",
    "setget",
    "breakpoint",
    "preload",
    "yield",
    "assert",
    "remote",
    "master",
    "puppet",
    "remotesync",
    "mastersync",
    "puppetsync",
    "PI",
    "TAU",
    "INF",
    "NAN"
  ]

isReservedKeyWord :: Text -> Bool
isReservedKeyWord = (`elem` reservedKeyWords)

isAllowedAsEVarRef :: Text -> Bool
isAllowedAsEVarRef x = not disallowedName && (isReservedKeyWord x == allowedKeyword)
  where
    allowedKeyword = x `elem` ["self", "preload", "assert", "yield", "PI", "TAU", "INF", "NAN"]
    disallowedName = x `elem` ["null", "true", "false"]

failParsing :: Parser ()
failParsing = failure Nothing (Set.fromList [])

failWhenReservedKeyword :: Text -> Parser ()
failWhenReservedKeyword x = when (isReservedKeyWord x) failParsing

failWhenIdIsDisallowedForEValRef :: Text -> Parser ()
failWhenIdIsDisallowedForEValRef x = unless (isAllowedAsEVarRef x) failParsing

pCharNotFollowedByEol :: Parser Char
pCharNotFollowedByEol = notFollowedBy eol *> L.charLiteral

pManyTillEol :: Parser Text
pManyTillEol = many pCharNotFollowedByEol <&> toS

pSomeTillEol :: Parser Text
pSomeTillEol = some pCharNotFollowedByEol <&> toS

pEol :: Parser ()
pEol = void eol

pCommentAsText :: Parser Text
pCommentAsText = optSc >> char '#' >> notFollowedBy (char '#') >> pManyTillEol & lexeme <?> "comment"

optScSkipComment :: Parser ()
optScSkipComment = optSc <* optional pCommentAsText

pCommentStatementAsText :: Parser Text
pCommentStatementAsText = pCommentAsText <* optScn

skipCommentStatement :: Parser ()
skipCommentStatement = void pCommentStatementAsText

optScnSkipComments :: Parser ()
optScnSkipComments = optScn <* many (try skipCommentStatement) <* optScn
