{-# LANGUAGE OverloadedStrings #-}

module Parser.StatementParser where

import Control.Monad (join, when)
import Data.Maybe (catMaybes, fromMaybe, maybeToList)
import Parser.Common
import Parser.Data
import Parser.ExpressionParser
  ( pExpr,
    pLit,
    pLitIntRaw,
    pLitStringRaw,
  )
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L
import Text.Megaparsec.Debug (dbg)
import Utils

pExtends :: Parser Node
pExtends = symbol "extends" >> pClassType <&> NExtends

pTool :: Parser Node
pTool = symbol "tool" $> NTool

pPass :: Parser Node
pPass = symbol "pass" $> NPass

pClassName :: Parser Node
pClassName = do
  symbol "class_name"
  pClassType <&> NClassName

pTypeAndAssignPart :: Parser (Maybe GType, Maybe Expr)
pTypeAndAssignPart = do
  (vType, expr) <- optional (try typedAssign <|> inferredAssign) <&> unzipMaybeTuple
  return (vType, expr)
  where
    inferredAssign = do
      optional $ char ':'
      expr <- optional assignExprPart
      return (Nothing, expr)
    typedAssign = do
      symbol ":"
      vType <- lexeme pType
      expr <- optional assignExprPart
      return (Just vType, expr)
    assignExprPart = do
      symbol "="
      pExpr

pVarSt :: Parser Node
pVarSt = do
  export <- optional pExport <?> "export"
  onReady <- optional pOnReady <?> "onready"
  symbol "var"
  name <- pVarId
  (vType, expr) <- try $ optional pTypeAndAssignPart <&> unzipMaybeTuple
  setget <- optional pSetget <?> "setget"
  let modifiers = [onReady, export, setget] & catMaybes
  return $ NVarDef name vType expr modifiers
  where
    pSetget = do
      symbol "setget"
      setter <- optional pFuncId
      getter <- try (optional (symbol "," >> pFuncId)) <|> (optional (symbol ",") $> Nothing)
      return $ SetgetVDM (setter, getter)
    pOnReady = symbol "onready" $> OnReadyVDM
    pExport = do
      symbol "export"
      details <- optional pDetailsList
      return $ ExportVDM $ fromMaybe ExportTypeSimple details
    pDetailsList = do
      symbol "("
      t <- pType
      let basic = return $ ExportTypeOther t
      r <-
        case t of
          GClassType "Array" -> pExportArray <|> basic
          GClassType "String" -> pExportEnum <|> basic
          _ -> basic
      symbol ")"
      return r
    pExportArray = pComma >> pType <&> ExportTypeArray
    pExportEnum = pComma >> pLitStringRaw `sepBy1` pComma <&> ExportTypeEnum
    pComma = symbol "," & void

pConstSt :: Parser Node
pConstSt = do
  symbol "const"
  name <- pVarId
  (vType, expr) <- try $ optional pTypeAndAssignPart <&> unzipMaybeTuple
  return $ NConstDef name vType expr

pMultilineStatementSupportingOnelineShorthand :: Show a => Parser a -> Parser [Node] -> Parser (a, [Node])
pMultilineStatementSupportingOnelineShorthand pHead pStat = try pMultiline <|> pOneline
  where
    pOneline = do
      h <- pHead
      ss <- moreWithMaybeCommentStatement pStat
      when ((length ss == 1) && (ss & head & isComment)) failParsing
      optScn
      return (h, ss)
    isComment (NComment _) = True
    isComment _ = False
    pMultiline = L.indentBlock optScnSkipComments pMultilineIB
    pMultilineIB = do
      h <- pHead
      c <- optional pComment <&> maybeToList
      return $ L.IndentSome Nothing (\ss -> return (h, c ++ concat ss)) (moreWithMaybeComment pStat)

pFuncDefArg :: Parser FuncDefArg
pFuncDefArg = p
  where
    p = do
      name <- pVarId
      (typ, defaultVal) <- optional pTypeAndAssignPart <&> unzipF
      return FuncDefArg {_name = name, _typ = join typ, _defaultValue = join defaultVal}

pFuncArgs :: Parser [FuncDefArg]
pFuncArgs = sepBy pFuncDefArg (symbol ",") & lexeme <?> "function definition arguments list"

pFuncRetPart :: Parser GType
pFuncRetPart = symbol "->" >> pType

pFuncDefHead :: Parser (FuncType, Text, [FuncDefArg], Maybe GType)
pFuncDefHead = do
  funcType <- optional (symbol "static") <&> fmap (const FuncTypeStatic) <&> fromMaybe FuncTypeMethod
  symbol "func"
  name <- pFuncId
  symbol "("
  args <- pFuncArgs
  symbol ")"
  ret <- optional pFuncRetPart
  symbol ":"
  return (funcType, name, args, ret)

pFunc :: Parser Node
pFunc = (p <?> "function definition") -- & dbg "pFunc"
  where
    p = do
      ((funcType, name, args, ret), ss) <- pMultilineStatementSupportingOnelineShorthand pFuncDefHead pFuncStatements
      return $ NFuncDef funcType name args ret ss

pIfHead :: Parser Expr
pIfHead = do
  symbol "if"
  cond <- pExpr
  symbol ":"
  return cond

pElseHead :: Parser ()
pElseHead = void $ symbol "else:"

pElifHead :: Parser Expr
pElifHead = do
  symbol "elif"
  cond <- pExpr
  symbol ":"
  return cond

pIf :: Parser Node
pIf = p <?> "if statement"
  where
    p = do
      if' <- pMultilineStatementSupportingOnelineShorthand pIfHead pFuncStatements
      elifs <- many $ pMultilineStatementSupportingOnelineShorthand pElifHead pFuncStatements
      else' <- optional $ pMultilineStatementSupportingOnelineShorthand pElseHead pFuncStatements <&> snd
      return $ NIf (if' : elifs) else'

pMatchHead :: Parser Expr
pMatchHead = symbol "match" >> pExpr <* symbol ":"

pMatchPattern :: Parser ([MatchPattern], [Node])
pMatchPattern = p
  where
    p = pMultilineStatementSupportingOnelineShorthand pPatty pFuncStatements
    pOnePatty = try pMPWildcard <|> try pMPLit <|> pMPVar
    pPatty = pOnePatty `sepBy1` symbol "," <* symbol ":"
    pMPLit :: Parser MatchPattern
    pMPLit = pLit <&> MPLit
    pMPVar :: Parser MatchPattern
    pMPVar = (pValidVarId `sepBy1` char '.') <&> MPVar
    pMPWildcard :: Parser MatchPattern
    pMPWildcard = string "_" $> MPWildcard

-- TODO: ? binding pattern
-- TODO: ? array pattern
-- TODO: ? dictionary pattern
pMatchStatement :: Parser Node
pMatchStatement = p <?> "match statement"
  where
    p = L.indentBlock scn pMultilineIB
    pMultilineIB = do
      h <- pMatchHead
      optScSkipComment
      return $ L.IndentSome Nothing (NMatch h >>> return) pMatchPattern

pForHead :: Parser (Text, Expr)
pForHead = do
  symbol "for"
  name <- pVarId
  symbol "in"
  expr <- pExpr
  symbol ":"
  return (name, expr)

pFor :: Parser Node
pFor = p <?> "for cycle"
  where
    p = do
      ((name, expr), ss) <- pMultilineStatementSupportingOnelineShorthand pForHead pFuncStatements
      return $ NFor name expr ss

pWhileHead :: Parser Expr
pWhileHead = do
  symbol "while"
  e <- pExpr
  symbol ":"
  return e

pWhile :: Parser Node
pWhile = p <?> "while cycle"
  where
    p = do
      (cond, ss) <- pMultilineStatementSupportingOnelineShorthand pWhileHead pFuncStatements
      return $ NWhile cond ss

pClassDefHead :: Parser Text
pClassDefHead = do
  symbol "class"
  name <- pClassId
  symbol ":"
  return name

pClass :: Parser Node
pClass = p <?> "inner class"
  where
    p = L.indentBlock scn pIB
    pIB = do
      name <- pClassDefHead
      return $ L.IndentSome Nothing (\ss -> return $ NInnerClass name (concat ss)) pInnerClassStatements

pComment :: Parser Node
pComment = pCommentAsText <&> NComment

pCommentStatement :: Parser Node
pCommentStatement = pCommentStatementAsText <&> NComment

pReturn :: Parser Node
pReturn = symbol "return" >> optional pExpr <&> NReturn

pAssign :: Parser Node
pAssign = p <?> "assignment"
  where
    p = do
      target <- pExpr
      op <- ["=", "+=", "-=", "*=", "/=", "%=", "&=", "|="] <&> symbol & choice
      pExpr <&> NAssign target op

pSignal :: Parser Node
pSignal = do
  symbol "signal"
  name <- pValidVarId
  args <- optional (parens $ pValidVarId `sepBy` symbol ",") <&> fromMaybe []
  return $ NSignal name args

pEnum :: Parser Node
pEnum = do
  symbol "enum"
  name <- optional pValidVarId
  symbol "{"
  optScnSkipComments
  items <- pItem `sepEndBy` (symbol "," >> optScnSkipComments)
  symbol "}"
  optScnSkipComments
  return $ NEnum name items
  where
    pItem = do
      name <- pValidVarId
      value <- optional $ symbol "=" >> pExpr <* optScnSkipComments
      optScnSkipComments
      return $ EnumItem name value

pBreak :: Parser Node
pBreak = symbol "break" $> NBreak

pContinue :: Parser Node
pContinue = symbol "continue" $> NContinue

pExpressionAsStatement :: Parser Node
pExpressionAsStatement = pExpr <&> NExpr

pDocText :: Parser DocItem
pDocText = pDocTextRaw <&> DText

-- TODO: support more of markdown - bold, italic, links, images?, quotes?
pDocTextRaw :: Parser [DTextPart]
pDocTextRaw = p
  where
    p = many (pRef <|> pCode <|> pPlain)
    pChar :: Parser Char
    pChar = notFollowedBy (void eol <|> void pRef <|> void pCode) >> anySingle
    pPlain :: Parser DTextPart
    pPlain = some (try pChar) <&> toS <&> DTextPlain
    pRef :: Parser DTextPart
    pRef = do
      string "[["
      id <- pVarId
      desc <- optional $ char '|' >> some (noneOf [']']) <&> toS
      string "]]"
      return $ DTextRef id desc
    pCode :: Parser DTextPart
    pCode = do
      char '`'
      chs <- manyTill anySingle (char '`')
      return $ DTextCode $ toS chs

pDocTypePrim :: Parser DType
pDocTypePrim = do
  first <- lowerChar
  name <- many alphaNumChar <&> toS
  return $ DTPrim (toS [first] <> name)

pDocTypeRef :: Parser DType
pDocTypeRef = do
  first <- upperChar
  name <- many alphaNumChar <&> toS
  args <- optional pArgs <&> fromMaybe []
  return $ DTRef (toS [first] <> name) args
  where
    pArgs = do
      string "<"
      typs <- pDocTypeRaw `sepBy1` symbol ","
      string ">"
      return typs

pDocTypeUnion :: Parser DType
pDocTypeUnion = do
  members <- pDocTypeRawWithoutUnion `sepBy1` symbol "|"
  when (length members < 2) failParsing $> DTUnion []
  return $ DTUnion members

pDocTypeRawWithoutUnion :: Parser DType
pDocTypeRawWithoutUnion = lexeme $ try pDocTypePrim <|> pDocTypeRef

pDocTypeRaw :: Parser DType
pDocTypeRaw = lexeme $ try pDocTypeUnion <|> pDocTypeRawWithoutUnion

pDocTypeLiteral :: Parser DType
pDocTypeLiteral = symbol "{" *> pDocTypeRaw <* symbol "}"

-- @param arr {Array<T>} the input array
pDocParam :: Parser DocItem
pDocParam = do
  symbol "@param"
  name <- pVarId
  typ <- pDocTypeLiteral
  note <- optional pDocTextRaw <&> fromMaybe []
  return $ DParam name typ note

-- @return {T | null}    a random item, or null for an empty array
pDocReturn :: Parser DocItem
pDocReturn = do
  symbol "@return"
  typ <- pDocTypeLiteral
  note <- optional pDocTextRaw <&> fromMaybe []
  return $ DReturn typ note

-- @typeParam T {any}      type of items in the array
pDocTypeParam :: Parser DocItem
pDocTypeParam = do
  symbol "@typeParam"
  name <- pVarId
  typ <- pDocTypeLiteral
  note <- optional pDocTextRaw <&> fromMaybe []
  return $ DTypeParam name typ note

pDocType :: Parser DocItem
pDocType = do
  symbol "@type"
  typ <- pDocTypeLiteral
  note <- optional pDocTextRaw <&> fromMaybe []
  return $ DType typ note

-- @example `sample_or_null_([])` returns always `null`
pDocExample :: Parser DocItem
pDocExample = symbol "@example" >> pDocTextRaw <&> DExample

pDocFileDocumentation :: Parser DocItem
pDocFileDocumentation = symbol "@fileDocumentation" $> DFileDocumentation

-- TODO: internal, ignore, category, related?
pDoc :: Parser Node
pDoc = p <?> "documentation comment"
  where
    p = ((symbol "##" >> pItem) `sepEndBy1` pEol) <* optScn <&> NDoc
    pItem = pDocParam <|> pDocReturn <|> pDocTypeParam <|> pDocType <|> pDocExample <|> pDocFileDocumentation <|> pDocText

-- Higher level parsers and utils
moreWithMaybeCommentStatement :: Parser [Node] -> Parser [Node]
moreWithMaybeCommentStatement p = do
  r <- p
  c <- optional (try pCommentStatement) <&> maybeToList
  return $ r ++ c

moreWithMaybeComment :: Parser [Node] -> Parser [Node]
moreWithMaybeComment p = do
  r <- p
  c <- optional pComment <&> maybeToList
  return $ r ++ c

semicolonDelimitedStatements :: Bool -> Parser Node -> Parser [Node]
semicolonDelimitedStatements eatNewLines p = pp -- & dbg "semicolonDelimitedStatements"
  where
    pp = do
      r <- try p `sepEndBy1` delim
      many delim
      bool optSc optScn eatNewLines
      return r
    delim = symbol ";"

pFuncStatement :: Parser Node
pFuncStatement =
  ( choice
      [ pCommentStatement,
        pVarSt,
        pPass,
        pReturn,
        try pAssign,
        pIf,
        try pExpressionAsStatement,
        pFor,
        pMatchStatement,
        pWhile,
        pBreak,
        pContinue
      ]
      <* optSc
  )
    <?> "statement (in function)"

pFuncStatements :: Parser [Node]
pFuncStatements = semicolonDelimitedStatements False pFuncStatement -- & dbg "pFuncStatements"

pInnerClassStatement :: Parser Node
pInnerClassStatement =
  (choice [try pFunc, try pConstSt] <* optSc) <|> pFuncStatement <?> "statement (in class or inner class)"

pInnerClassStatements :: Parser [Node]
pInnerClassStatements = semicolonDelimitedStatements False pInnerClassStatement

pTopLevelStatement :: Parser Node
pTopLevelStatement = (p <?> "top level statement") -- & dbg "pTopLevelStatement"
  where
    p =
      (choice [try pTool, try pExtends, try pClassName, try pClass, try pSignal, try pEnum, try pDoc] <* optScn)
        <|> (pInnerClassStatement <* optScn)

pTopLevelStatements :: Parser [Node]
pTopLevelStatements = semicolonDelimitedStatements True pTopLevelStatement -- & dbg "pTopLevelStatements"
