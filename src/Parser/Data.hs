module Parser.Data where

import qualified Data.Text as T
import Data.Void (Void)
import Text.Megaparsec
import Utils

type Parser = Parsec Void Text

data PrimType
  = PTBool
  | PTInt
  | PTFloat
  | PTVoid
  deriving (Show, Eq)

data GType
  = GClassType Text
  | GPrimType PrimType
  deriving (Show, Eq)

data FuncDefArg
  = FuncDefArg
      { _name :: Text,
        _typ :: Maybe GType,
        _defaultValue :: Maybe Expr
      }
  deriving (Show, Eq)

data Literal
  = LString Text
  | LInt Int
  | LFloat Float
  | LNull
  | LBool Bool
  | LArray [Expr]
  | LDictionary [(Expr, Expr)]
  | LGetNode Text
  deriving (Show, Eq)

data Expr
  = ELit Literal
  | EVarRef Text
  | EAttrRef Expr Text
  | EFuncCall Expr [Expr]
  | EIndexRef Expr Expr
  | EANeg Expr
  | EAAdd Expr Expr
  | EASub Expr Expr
  | EAMul Expr Expr
  | EADiv Expr Expr
  | EARem Expr Expr
  | ECLT Expr Expr
  | ECGT Expr Expr
  | ECEQ Expr Expr
  | ECNEQ Expr Expr
  | ECLTE Expr Expr
  | ECGTE Expr Expr
  | EBNot Expr
  | EBAnd Expr Expr
  | EBOr Expr Expr
  | EIn Expr Expr
  | EGType GType
  | EIs Expr Expr
  | EAs Expr Expr
  | EIf Expr Expr Expr -- T C F
  | EBitShiftLeft Expr Expr
  | EBitShiftRight Expr Expr
  | EBitAnd Expr Expr
  | EBitOr Expr Expr
  | EBitXor Expr Expr
  | EBitNot Expr
  deriving (Show, Eq)

data MatchPattern
  = MPLit Literal
  | MPVar [Text]
  | MPWildcard
  deriving (Show, Eq)

data ExportType
  = ExportTypeSimple
  | ExportTypeEnum [Text]
  | ExportTypeArray GType
  | ExportTypeOther GType
  deriving (Show, Eq)

data VarDefModifier
  = SetgetVDM (Maybe Text, Maybe Text)
  | OnReadyVDM
  | ExportVDM ExportType
  deriving (Show, Eq)

data EnumItem
  = EnumItem Text (Maybe Expr)
  deriving (Show, Eq)

data FuncType
  = FuncTypeMethod
  | FuncTypeStatic
  deriving (Show, Eq)

data DType
  = DTPrim Text
  | DTRef Text [DType]
  | DTUnion [DType]
  deriving (Show, Eq)

data DTextPart
  = DTextPlain Text
  | DTextCode Text
  | DTextRef Text (Maybe Text)
  deriving (Show, Eq)

data DocItem
  = DText [DTextPart]
  | DParam Text DType [DTextPart]
  | DReturn DType [DTextPart]
  | DTypeParam Text DType [DTextPart]
  | DType DType [DTextPart]
  | DExample [DTextPart]
  | DFileDocumentation
  deriving (Show, Eq)

-- TODO: add position
data Node
  = NExtends GType
  | NTool
  | NPass
  | NComment Text
  | NClassName GType
  | NVarDef Text (Maybe GType) (Maybe Expr) [VarDefModifier]
  | NConstDef Text (Maybe GType) (Maybe Expr)
  | NFuncDef FuncType Text [FuncDefArg] (Maybe GType) [Node]
  | NReturn (Maybe Expr)
  | NAssign Expr Text Expr
  | NInnerClass Text [Node]
  | NExpr Expr
  | NIf [(Expr, [Node])] (Maybe [Node]) -- [(if), (elif #1), (elif #2), ...] else
  | NFor Text Expr [Node]
  | NMatch Expr [([MatchPattern], [Node])]
  | NSignal Text [Text]
  | NEnum (Maybe Text) [EnumItem]
  | NWhile Expr [Node]
  | NBreak
  | NContinue
  | NDoc [DocItem]
  deriving (Show, Eq)
