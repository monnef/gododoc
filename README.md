# GodoDoc

A documentation generator for GDScript projects. Its output is in a [Markdown format](https://daringfireball.net/projects/markdown/basics).

Project status: **Alpha**

Basic functionality is implemented (parses real-code project ~8.5k LoC, generates docs for a library ~2.5k LoC). There is a risk of bugs and a few parts of API will most likely change.

# Example
[This markdown file](docs-example/index.md) was generated using [this config](gododoc.yaml) from [this source file](data/documented/func.gd).

# Installation

## Binary

1) Download an archive from [releases](../../releases).
2) Unpack the archive to a directory which is in your path environment variable.

## From source code

### Requirements

* [stack](http://haskellstack.org)

### Install to stack bin directory

Clone this repository and navigate inside its directory.

```sh
$ stack install
```

# Usage

It is assumed that you have GodoDoc installed and you are in a directory with your GDScript project.

## Configuration

Generate a configuration file:

```sh
$ gododoc --generate-config
```

Customize the configuration file `gododoc.yaml` to your linking.

## Generating docs

```sh
$ gododoc
```

GodoDoc will look for all `.gd` files in the configured `inputDir` and generate a markdown file in `outputDir`.

## Documentation comments

Documentation comments start with `##`.

```gdscript
## I am a doc comment.
```

### Code

```gdscript
## Code example: `print("Hello Internet!")`
```

### Link

```gdscript
## Link to other method in a same file: [[map_fn]]
## Link to other file: [[GGArray]]
## Link with custom text: [[map_fn|I AM THE LINK]]
```

### Function

```gdscript
## Same as [[bool_]], but `on_false`/`on_true` are functions.
## Only selected function will be called and its return value will be returned from `bool_lazy`.
## @typeParam T {any} Return type
## @param cond {bool} Condition
## @param on_false {FuncLike<T>} Function to call and return its result when `cond` is `false`
## @param on_true {FuncLike<T>} Function to call and return its result when `cond` is `true`
## @return {T}
func bool_lazy_(cond: bool, on_false, on_true): return GGI.bool_lazy_(cond, on_false, on_true)
var bool_lazy = funcref(self, "bool_lazy_")
```

### File
Should be a first doc comment.

```gdscript
## If some function is undocumented here (e.g. [[size_]]), please see documentation of [[GGArray]].
## @fileDocumentation
```

## Tips

### Markdown preview

You can use [markserv](https://github.com/markserv/markserv) to serve your markdown documentation, so you can easily preview it in your browser. It also supports live-reload (browser extension is required).

```sh
$ markserv docs/index.md
```

### Automatically generate docs on source change

You can use [entr](http://eradman.com/entrproject) to watch your `.gd` files and run GodoDoc on changes automatically.

```sh
$ find -iname '*.gd' | entr -s "gododoc"
```

# Development

## Tests

Utils, crawler and parser are covered by tests.

```sh
$ clear ; stack test --ta '-q'
```

## Running

```sh
$ stack run -- --help
```

or

```sh
$ stack run gododoc-dev -- --help
```

if `stack run` refuses to run first executable (`gododoc-dev`) and instead runs a second one (production build with optimizations).

## TODO
* links to other files (classes) in multi-file mode
* class docs (`class_name`, instead of just file docs), support for `@typeParam`
* multiline examples?
* check if `name` in `@param` is valid
* linkify types in functions
* `@category`
* standalone builds?
* include all classes and functions by default (currently only documented ones are emitted)
  * `@internal` and/or `@hidden`
  * automatic marking as internal of `func`s/`var`s starting with `_`
* `FuncRef` companions?
* links to sources?
  * parser: record source position for all or just `func`/`var`/`const`/`class_name` nodes
* parser: record all comments?
* support for classes/functions defined outside of project (e.g. Godot docs)?
* config doesn't requiring all fields? https://reasonablypolymorphic.com/blog/higher-kinded-data/
